/*
 * @Author: AiLjx
 * @Date: 2022-06-29 22:54:29
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 23:33:34
 */
import React from "react";
import Add from "./components/Add";
import QueryList from "./components/QueryList";

import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";

const client = new ApolloClient({
    uri: "/graphql",
});
export default function App() {
    //  暂存QueryList组件的refetch
    let QueryListRefetch = null;
    return (
        <ApolloProvider client={client}>
            <div>
                <Add QueryListRefetch={() => QueryListRefetch()}></Add>
                <QueryList
                    AppCb={(refetch) =>
                        (QueryListRefetch = refetch)
                    }></QueryList>
            </div>
        </ApolloProvider>
    );
}
