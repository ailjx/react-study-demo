/*
 * @Author: AiLjx
 * @Date: 2022-06-29 22:54:51
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 23:41:28
 */
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

export default function Delete({ id, QueryListRefetch }) {
    const myquery = gql`
        mutation ($id: String!) {
            deleteFilm(id: $id)
        }
    `;

    return (
        <Mutation mutation={myquery}>
            {(deleteFilm, { data }) => {
                return (
                    <div>
                        <button
                            onClick={() => {
                                deleteFilm({
                                    variables: {
                                        id: id,
                                    },
                                }).then((res) => {
                                    // 调用QueryList组件的refetch，使其重新发送请求查询数据
                                    QueryListRefetch();
                                    console.log("删除结果：", res.data);
                                    return res;
                                });
                            }}>
                            删除
                        </button>
                    </div>
                );
            }}
        </Mutation>
    );
}
