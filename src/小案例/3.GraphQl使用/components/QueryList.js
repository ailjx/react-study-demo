/*
 * @Author: AiLjx
 * @Date: 2022-06-29 22:55:08
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 23:42:48
 */

import { Query } from "react-apollo";
import gql from "graphql-tag";
import Delete from "./Delete";

export default function QueryList({ AppCb }) {
    const query = gql`
        query {
            getNowplayingList {
                id
                name
                poster
                price
            }
        }
    `;
    return (
        <Query query={query}>
            {/* 调用refetch方法可重新发起请求 */}
            {({ loading, data, refetch }) => {
                console.log("查询结果", data);
                // 将refetch方法传给父组件App，供其它组件调用
                AppCb(refetch);
                return (
                    <>
                        {!loading && (
                            <div>
                                {data.getNowplayingList.map((item) => (
                                    <div
                                        key={item.id}
                                        style={{
                                            border: "1px solid black",
                                            margin: "20px",
                                            padding: "10px",
                                        }}>
                                        <li>
                                            名字：
                                            {item.name}
                                        </li>
                                        <li>封面： {item.poster}</li>
                                        <li>价格： {item.price}</li>
                                        <Delete
                                            id={item.id}
                                            QueryListRefetch={() =>
                                                refetch()
                                            }></Delete>
                                    </div>
                                ))}
                            </div>
                        )}
                    </>
                );
            }}
        </Query>
    );
}
