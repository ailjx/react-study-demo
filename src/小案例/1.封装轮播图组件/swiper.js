import React, { Component } from "react";
// 基于Swiper封装react轮播组件
import Swiper, { Navigation, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

export default class RSwiper extends Component {
    componentDidUpdate() {
        console.log(999);
        new Swiper(".swiper", {
            // 分页器
            modules: [Navigation, Pagination],
            pagination: {
                el: ".swiper-pagination",
            },
            // 前进后退按钮
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            // 循环(loop)模式
            loop: this.props.loop,
        });
    }
    render() {
        return (
            <div
                className='swiper'
                style={{
                    height: this.props.height,
                    background: this.props.background,
                }}>
                <div className='swiper-wrapper'>{this.props.children}</div>
                {this.props.pagination && (
                    <div className='swiper-pagination'></div>
                )}
                {this.props.navigation && (
                    <>
                        <div className='swiper-button-prev'></div>
                        <div className='swiper-button-next'></div>
                    </>
                )}
            </div>
        );
    }
}
