import React, { Component } from "react";
import RSwiper from "./swiper";
import RSwiperItem from "./swiperItem";
export default class App extends Component {
    state = {
        list: [],
    };
    //性能优化
    shouldComponentUpdate(nextProps, nextState) {
        if (
            JSON.stringify(nextState.list) === JSON.stringify(this.state.list)
        ) {
            return false;
        }
        return true;
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState({
                list: ["1", "2", "3", "4", "5", "6"],
            });
        }, 1000);
    }
    render() {
        console.log("App");
        return (
            <>
                <RSwiper
                    height={"500px"}
                    loop={true}
                    pagination={true}
                    navigation={true}>
                    {this.state.list.map((item) => (
                        <RSwiperItem key={item}>{item}</RSwiperItem>
                    ))}
                </RSwiper>
                <button
                    onClick={() => {
                        this.setState({
                            list: ["999", "666", "555", "222", "111"],
                        });
                    }}>
                    更新state状态
                </button>
            </>
        );
    }
}
