import React, { useEffect } from "react";
import { connect } from "react-redux";
import { getListAction } from "./actionCreator/ListActionCreator";
function Home({ list, getListAction }) {
    useEffect(() => {
        if (list.length === 0) {
            // 发送请求
            console.log("发送请求");

            getListAction();
        } else {
            // 读取缓存
            console.log("读取缓存");
        }
    }, [list, getListAction]);

    return (
        <div>
            Home页面
            <ul>
                {list.map((item) => (
                    <li key={item}>{item}</li>
                ))}
            </ul>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        list: state.ListReducer.list,
    };
};

const mapDispatchToProps = {
    getListAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
