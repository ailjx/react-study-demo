/**
 * 安装：npm install redux-persist
 * 在使用redux持久化之前需要先使用react-redux
 * 使用步骤：
 *      1.在index.js入口文件用PersistGate 包裹App组件
 *      2.再使用React-Redux导出的Provider包裹App组件
 */

// 注意：查看本案例前确保index.js入口文件是：
// import { Provider } from "react-redux";
// import { store, persistor } from "./6.redux状态管理/8.redux持久化/redux/store";
// import { PersistGate } from "redux-persist/integration/react";
// <Provider store={store}>
// <PersistGate loading={null} persistor={persistor}>
//     <App />
// </PersistGate>
// </Provider>

import React from "react";
import Home from "./Home";

import { connect } from "react-redux";

function App({ isShow, hide, show }) {
    return (
        <div>
            <button onClick={hide}>隐藏标题</button>
            <button onClick={show}>显示标题</button>
            {isShow && <Home></Home>}
        </div>
    );
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        isShow: state.HomeReducer.isShow,
    };
};

const mapDispatchToProps = {
    hide() {
        return {
            type: "hide",
        };
    },
    show: () => {
        return {
            type: "show",
        };
    },
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
