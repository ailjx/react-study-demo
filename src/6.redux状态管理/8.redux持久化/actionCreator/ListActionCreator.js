function getListAction() {
    return new Promise(function (resolve, reject) {
        //做一些异步操作
        setTimeout(() => {
            console.log("模拟请求发送成功~~~~~~~~~~~~~~~");
            const res = [1, 2, 3];
            resolve({
                type: "change-list",
                payLoad: res,
            });
        }, 1000);
    });
}

export { getListAction };
