import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";
import reduxPromise from "redux-promise";
import HomeReducer from "../reducers/TitleReducer";
import ListReducer from "../reducers/ListReducer";

// 引入redux持久化文件：redux-persist
import { persistStore, persistReducer } from "redux-persist"; // 持久化操作
import storage from "redux-persist/lib/storage"; // 持久化操作

const reducer = combineReducers({
    HomeReducer,
    ListReducer,
});

const persistConfig = {
    key: "ReactDemo", //存放到localStorage的key
    storage,
    whitelist: ["ListReducer"], // 白名单:只有ListReducer会被持久化存到localStorage
    // blacklist: ['HomeReducer'] // 黑名单:只有HomeReducer不会被持久化存到localStorage
    // 不加whitelist和blacklist,默认全部都会被持久化存到localStorage
}; // 持久化配置

const persistedReducer = persistReducer(persistConfig, reducer); // 持久化操作

// redux开发者工具：https://github.com/zalmoxisus/redux-devtools-extension
// 使用composeEnhancers配合浏览器插件使用
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    // reducer,
    persistedReducer, // 持久化操作
    // 使用applyMiddleware应用中间件
    composeEnhancers(applyMiddleware(reduxThunk, reduxPromise))
);

const persistor = persistStore(store); // 持久化操作

// export default store

export { store, persistor }; // 持久化操作
