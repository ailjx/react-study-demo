import { createStore } from "redux";

// 默认状态对象
const state = {
    isShow: true,
    //...
};
// 处理函数
// prevState是老的状态，利用es6语法使prevState = state来初始化状态（定义默认状态）
const reducer = (prevState = state, action) => {
    //不能直接修改原状态，需要先深copy一份
    let newState = { ...prevState };
    switch (action.type) {
        case "hide":
            newState.isShow = false;
            return newState;
        case "show":
            newState.isShow = true;
            return newState;
        default:
            return prevState;
    }
};
// 不使用prevState = state时，也可以将state传给createStore第二个参数来初始化状态
// const store = createStore(reducer,state);
const store = createStore(reducer);
export default store;
/**
 * redux设计和使用的三大原则
 * 1.state以单一对象存储再store对象中
 * 2.state只读（每次都返回一个新的对象）
 * 3.使用纯函数reducer执行state更新（纯函数：1.对外界没有副作用，不影响外界变量或对象等等 2.同样的输入得到同样的输出）
 * redux理念：将状态和处理状态的逻辑抽离开来，加强解耦，便于维护
 */
