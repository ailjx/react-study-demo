// 使用redux之前，应先理解1.基础/9.1发布订阅模式通信.js和2.Hooks/7.useReducer.js的使用
// redux和hooks中useReducer类似
// 安装：npm i redux
import React, { useState, useEffect } from "react";
import Home from "./Home";
import store from "./redux/store";

export default function App() {
    // store.getState().isShow 获取store状态中的isShow
    const [isShow, setisShow] = useState(store.getState().isShow);
    useEffect(() => {
        // 在组件初始化时订阅
        store.subscribe(() => {
            setisShow(store.getState().isShow);
        });
    }, []);

    return (
        <div>
            {isShow && <h1>App标题</h1>}
            <Home></Home>
        </div>
    );
}
// Redux与Mobx区别：
// Mobx背后的哲学很简单: 任何源自应用状态的东西都应该自动地获得。（不需要手动订阅和发布）
// Mobx写法上更偏向于OOP（面向对象编程）， 对一份数据直接进行修改操作，不需要始终返回一个新的数据
// 并非单一store,可以多store，并且学习成本底
// Mobx利用getter和setter来收集组件的数据依赖关系，从而在数据发生变化的时
// 候精确知道哪些组件需要重绘，在界面的规模变大的时候，往往会有很多细粒度更新。（与vue类似）
// Mobx提供的约定及模版代码很少，代码编写很自由，如果不做一些约定，比较容易导致团队代码风格不统一
// Redux默认以JavaScript原生对象形式存储数据，而Mobx使用可观察对象

// Redux与react设计风格很匹配（都是数据不可变，单向数据流），
// Redux是Flux的一种实现，react官方也有Flux的实现：Facebook Flux，但它过于难用
// 这也是react和redux两者结合使用较多的原因，Redux用一个单独的常量状态树（state对象）保存这一整个应用的
// 状态，这个对象不能直接被改变。当一些数据变化了，一个新的对象就会被创建（使用actions和reducers），这
// 样就可以进行数据追踪，实现时光旅行
// Redux比较复杂，但中间件很多，模板代码多，约束强

// Flux
// Flux 是一种架构思想，专门解决软件的结构问题。它跟MVC架构是同一类东西，但是更加简单和
// 清晰。Flux存在多种实现(至少15种)：https://github.com/voronianski/flux-comparison
// 1.用户访问view
// 2.View发出用户的Action
// 3.Dispatcher收到Action,要求Store进行相应的更新
// 4.Store更新后，发出一个"change'"事件
// 5.View收到"change"事件后，更新页面

// Facebook Flux是用来构建客户端Web应用的应用架构。它利用单向数据流的方式来组合React
// 中的视图组件。它更像一个模式而不是一个正式的框架，使开发者不需要太多的新代码就可以快
// 速的上手Flux。
