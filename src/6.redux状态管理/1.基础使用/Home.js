import React from "react";
import store from "./redux/store";
export default function Home() {
    const hide = () => {
        // dispatch为订阅发布模式中的发布
        // 调用dispatch后，store中reducer第二个参数action能接收dispatch的参数，并在reducer内部进行逻辑处理
        // 即dispatch实际接收的就是action对象
        store.dispatch({
            // 格式固定，必须是type
            type: "hide",
        });
    };

    const show = () => {
        store.dispatch({
            type: "show",
        });
    };

    return (
        <div>
            <button onClick={hide}>隐藏标题</button>
            <button onClick={show}>显示标题</button>
        </div>
    );
}
