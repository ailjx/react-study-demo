// 因为action只能是js对象，所以actionCreator函数必须return的是action类型的对象
function getListAction() {
    // 模拟发送请求
    setTimeout(() => {
        let res = [1, 2, 3];
        // return放到这里也不行，因为这个在异步中的return并不再是getListAction函数的return了
        return {
            type: "change-list",
            payLoad: res,
        };
    }, 1000);
    // return放到这里不行
    // return {
    //     type: "change-list",
    //     payLoad: res, // 报：'res' is not defind
    // };
}

// 下面用await等待promise异步也不行，无论尝试什么方法action内都无法正常进行异步操作
// async function getListAction() {
//     let res = [];
//     let action = await new Promise(function (resolve, reject) {
//         //做一些异步操作
//         setTimeout(() => {
//             console.log("模拟请求发送成功~~~~~~~~~~~~~~~");
//             res = [1, 2, 3];
//             resolve({
//                 type: "change-list",
//                 payLoad: res,
//             });
//         }, 1000);
//     });
//     console.log(action);
//     return {
//         type: "change-list",
//         payLoad: res,
//     };
// }
export { getListAction };
