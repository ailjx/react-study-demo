import { createStore, combineReducers } from "redux";
import ListReducer from "../reducers/ListReducer";
const reducer = combineReducers({
    ListReducer,
});

const store = createStore(reducer);
export default store;
