// 在redux里，action仅仅是携带了数据的普通js对象。actionCreator（action创建者，即创建action的函数）返回的值是这个action类型的
// 对象。然后通过store.dispatch()进行分发。即store.dispatch()最终只能接收一个对象，同步的情况下一切都很完美，但是reducer无法处理异
// 步的情况。

// 试想一个应用场景：
// 这里这个App组件是一个将来能够被频繁隐藏和显示的组件，并且App内部需要发送请求获取数据
// 为了避免重复请求，只需在组件第一次显示的时候发送请求，之后组件被隐藏销毁再被创建时读取缓存而不是重新发送请求
// 这就可以将数据存到redux中，因为redux的期望就是将状态和处理状态的逻辑代码抽离开来，而不是全写在组件中
// 所以我们会想要在actionCreator中发送请求，这时就会遇到reducer无法处理异步的情况

import React, { useEffect, useState } from "react";
import store from "./redux/store";
// 将actionCreator代码抽离出来
import { getListAction } from "./actionCreator/ListActionCreator";

export default function App() {
    const [list, setlist] = useState(store.getState().ListReducer.list);

    useEffect(() => {
        if (store.getState().ListReducer.list.length === 0) {
            // 发送请求
            console.log("发送请求");
            // dispatch最终只能接收一个action对象，对象内包含type和我们需要发布的数据，
            // 这样的话我们就可以写一个return action的actionCreator函数，然后将异步请求写入其中，
            // 如：getListAction函数，见：/actionCreator/ListActionCreator
            store.dispatch(getListAction());
        } else {
            // 读取缓存
            console.log("读取缓存");
        }
        store.subscribe(() => {
            setlist(store.getState().ListReducer.list);
        });
    }, []);

    return (
        <div>
            Home页面
            <ul>
                {list.map((item) => (
                    <li key={item}>item</li>
                ))}
            </ul>
        </div>
    );
}

// 此案例最终运行效果会报错，在使用异步的情况下，无论我们的actionCreator怎样写，
// reducer都无法处理，因为dispatch(actionCreator())实质是reducer(prevState,actionCreator()),
// reducer无法对这个异步actionCreator返回的action进行正确处理
// 为了能够使reducer处理异步，我们将引入redux中间件，使actionCreator最终返回结果可以不仅仅是一个对象，
// 见下一节用同样的案例使用中间件解决redux异步问题
