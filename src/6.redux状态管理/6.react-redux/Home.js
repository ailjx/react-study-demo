import React, { useEffect } from "react";
import { connect } from "react-redux";
// import store from "./redux/store";
// 将actionCreator代码抽离出来
import { getListAction } from "./actionCreator/ListActionCreator";
function Home({ list, getListAction }) {
    // const [list, setlist] = useState(store.getState().ListReducer.list);
    useEffect(() => {
        // if (store.getState().ListReducer.list.length === 0) {
        if (list.length === 0) {
            // 发送请求
            console.log("发送请求");
            // store.dispatch(getListAction());

            // 直接调用connect传来的actionCreator函数，dispatch由connect来做
            // 不用调用过actionCreator函数后还要手动调用dispatch了
            getListAction();
        } else {
            // 读取缓存
            console.log("读取缓存");
        }
        // 不再需要订阅和取消订阅
        // const unsubscribe = store.subscribe(() => {
        //     setlist(store.getState().ListReducer.list);
        // });
        // return () => {
        //     unsubscribe();
        // };
    }, [list, getListAction]);

    return (
        <div>
            Home页面
            <ul>
                {list.map((item) => (
                    <li key={item}>{item}</li>
                ))}
            </ul>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        list: state.ListReducer.list,
    };
};

const mapDispatchToProps = {
    // 异步的actionCreator也可以直接放入
    getListAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

// 使用react-redux，会发现组件内不再含有redux的任务逻辑，以及不再有自己的状态，全部都有props控制
// 这就是react倡导的无状态组件
