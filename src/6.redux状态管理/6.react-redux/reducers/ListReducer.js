const ListReducer = (
    prevState = {
        list: [],
    },
    action
) => {
    let newState = { ...prevState };
    switch (action.type) {
        case "change-list":
            newState.list = action.payLoad;
            return newState;
        default:
            return prevState;
    }
};
export default ListReducer;
