import { createStore, combineReducers } from "redux";
import TitleReducer from "../reducers/TitleReducer";
import FooterReducer from "../reducers/FooterReducer";

// 使用combineReducers合并多个reducer
// 注意：1.多个reducer中action的type不能有相同的，不然多个reducer会冲突（但state状态名可以相同，如本案例中isShow）
const reducer = combineReducers({
    TitleReducer, // 直接使用TitleReducer为名
    footer: FooterReducer, //重命名为footer
});
// 合并reducer后调用state.getState()访问状态时需要点出对应的reducer，如：
// store.getState().TitleReducer.isShow和store.getState().footer.isShow
const store = createStore(reducer);
export default store;
