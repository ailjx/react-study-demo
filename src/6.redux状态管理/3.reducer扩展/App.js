import React, { useState, useEffect } from "react";
import Home from "./Home";
import store from "./redux/store";

export default function App() {
    const [isShow, setisShow] = useState(store.getState().TitleReducer.isShow);
    const [footerIsShow, setfooterIsShow] = useState(
        store.getState().footer.isShow
    );
    useEffect(() => {
        store.subscribe(() => {
            setisShow(store.getState().TitleReducer.isShow);
            setfooterIsShow(store.getState().footer.isShow);
        });
    }, []);
    console.log(999);
    return (
        <div>
            {isShow && <h1>App标题</h1>}
            <Home></Home>
            {footerIsShow && <h1>App页脚</h1>}
        </div>
    );
}
