const FooterReducer = (
    prevState = {
        isShow: true,
    },
    action
) => {
    let newState = { ...prevState };
    switch (action.type) {
        case "Fhide":
            newState.isShow = false;
            return newState;
        case "Fshow":
            newState.isShow = true;
            return newState;
        default:
            return prevState;
    }
};
export default FooterReducer;
