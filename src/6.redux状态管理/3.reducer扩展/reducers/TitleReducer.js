const TitleReducer = (
    prevState = {
        isShow: true,
    },
    action
) => {
    let newState = { ...prevState };
    switch (action.type) {
        case "Thide":
            newState.isShow = false;
            return newState;
        case "Tshow":
            newState.isShow = true;
            return newState;
        default:
            return prevState;
    }
};
export default TitleReducer;
