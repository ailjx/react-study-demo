import React from "react";
import store from "./redux/store";
export default function Home() {
    const hide = () => {
        store.dispatch({
            type: "Thide",
        });
    };

    const show = () => {
        store.dispatch({
            type: "Tshow",
        });
    };

    const Fhide = () => {
        store.dispatch({
            type: "Fhide",
        });
    };

    const Fshow = () => {
        store.dispatch({
            type: "Fshow",
        });
    };

    return (
        <div>
            <button onClick={hide}>隐藏标题</button>
            <button onClick={show}>显示标题</button>
            <button onClick={Fhide}>隐藏页脚</button>
            <button onClick={Fshow}>显示页脚</button>
        </div>
    );
}
