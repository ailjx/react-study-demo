// 使用手写的redux源码替代redux实现基础使用的案例
import React, { useState, useEffect } from "react";
import Home from "./Home";
import store from "./redux/store";

export default function App() {
    // store.getState().isShow 获取store状态中的isShow
    const [isShow, setisShow] = useState(store.getState().isShow);
    useEffect(() => {
        // 在组件初始化时订阅
        store.subscribe(() => {
            setisShow(store.getState().isShow);
        });
    }, []);

    return (
        <div>
            {isShow && <h1>App标题：手写redux基本源码</h1>}
            <Home></Home>
        </div>
    );
}
