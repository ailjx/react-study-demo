/**
 * 根据发布订阅模式和redux用法反推出redux基本源码
 */
/**
 *
 * @param {reducer} reducer
 * @param {默认状态} DefaultState
 * @returns
 */
function createStore(reducer, DefaultState) {
    // 存放订阅者
    let list = [];
    // 因为reducer会return出新状态(第一次return的是初始化状态),所以直接将state=reducer return的结果
    // 给reducer传一个{}的目的是,初始化执行时若不传{},reducer中action为undefined,会报:无法从Undefined中读取type
    let state = DefaultState || reducer(DefaultState, {});

    // 订阅函数
    function subscribe(callback) {
        // 添加订阅者
        list.push(callback);
    }

    // 发布函数
    function dispatch(action) {
        // 调用reducer,传入老状态和发布者传来的action,之后再将reducer返回的新状态赋值给state
        state = reducer(state, action);
        // 遍历通知所有订阅者
        list.forEach((cb) => cb && cb());
    }

    // 获取状态
    function getState() {
        return state;
    }

    return {
        getState,
        dispatch,
        subscribe,
    };
}
export { createStore };
