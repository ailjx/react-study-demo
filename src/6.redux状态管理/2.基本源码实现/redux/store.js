// 使用我们自己实现的简易redux
import { createStore } from "./source";

// 默认状态对象
const state = {
    isShow: true,
    //...
};
// 处理函数
// prevState是老的状态，利用es6语法使prevState = state来初始化状态（定义默认状态）
const reducer = (prevState = state, action) => {
    //不能直接修改原状态，需要先深copy一份
    let newState = { ...prevState };
    switch (action.type) {
        case "hide":
            newState.isShow = false;
            return newState;
        case "show":
            newState.isShow = true;
            return newState;
        default:
            return prevState;
    }
};
// 不使用prevState = state时，也可以将state传给createStore第二个参数来初始化状态
// const store = createStore(reducer,state);
const store = createStore(reducer);
export default store;
