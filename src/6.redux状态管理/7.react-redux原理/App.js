// react-redux的Provider组件，可以让容器组件拿到state ， 使用了context
// 这里主要手写一下connect高阶组件的基本原理：使用高阶组件的知识

// 高阶组件（HOC）构建与应用：
// HOC不仅仅是一个方法，确切说应该是一个组件工厂，获取低阶组件，生成高阶组件。
// (1) 代码复用，代码模块化
// (2) 增删改props
// (3) 渲染劫持
import React from "react";

function App(props) {
    return (
        <div>
            App
            <h1>{props.name}</h1>
            <button onClick={props.show}>展示</button>
        </div>
    );
}

const mapStateToProps = () => {
    return {
        name: "老六",
    };
};

const mapDispatchToProps = {
    show: () => alert("展示"),
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

// 定义高阶函数：connect
function connect(cb, obj) {
    // 代码复用，代码模块化
    let state = cb();
    // connect返回一个函数，该函数接收一个低阶组件参数
    return (Component) => {
        // 接收到低阶组件后返回一个处理过后的函数式组件
        // 这个处理过后的组件导出后，将来被使用时可能会接收一些props，如路由组件Route传递的props
        // 需要将这些props也传给原低阶组件
        return (props) => {
            return (
                // 渲染劫持：增添样式
                <div style={{ color: "red" }}>
                    {/* 增删改props */}
                    <Component {...state} {...props} {...obj}></Component>
                </div>
            );
        };
    };
}
