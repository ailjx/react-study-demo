// 解决redux使用异步的问题可以使用两个中间件：redux-thunk、 redux-promise
// 安装：npm i redux-thunk、 npm i redux-promise
import React, { useState, useEffect } from "react";
import Home from "./Home";
import store from "./redux/store";

export default function App() {
    const [isShow, setisShow] = useState(store.getState().HomeReducer.isShow);
    useEffect(() => {
        store.subscribe(() => {
            setisShow(store.getState().HomeReducer.isShow);
        });
    }, []);

    const hide = () => {
        store.dispatch({
            type: "hide",
        });
    };

    const show = () => {
        store.dispatch({
            type: "show",
        });
    };
    return (
        <div>
            <button onClick={hide}>隐藏标题</button>
            <button onClick={show}>显示标题</button>
            {isShow && <Home></Home>}
        </div>
    );
}
