// 使用redux-thunk：使actionCreator最终返回的可以是一个函数，并且该函数具有一个和dispatch一样作用的参数
// function getListAction() {
//     return (dispatch) => {
//         // 模拟发送请求
//         setTimeout(() => {
//             console.log("模拟请求发送成功~~~~~~~~~~~~~~~");
//             const res = [1, 2, 3];
//             dispatch({
//                 type: "change-list",
//                 payLoad: res,
//             });
//         }, 1000);
//     };
// }

// 使用redux-promise，使actionCreator最终返回的可以是一个promise
function getListAction() {
    return new Promise(function (resolve, reject) {
        //做一些异步操作
        setTimeout(() => {
            console.log("模拟请求发送成功~~~~~~~~~~~~~~~");
            const res = [1, 2, 3];
            resolve({
                type: "change-list",
                payLoad: res,
            });
        }, 1000);
    });
}

// ES6写法
// async function getListAction() {
//     let action = await new Promise(function (resolve, reject) {
//         //做一些异步操作
//         setTimeout(() => {
//             console.log("模拟请求发送成功~~~~~~~~~~~~~~~");
//             const res = [1, 2, 3];
//             resolve({
//                 type: "change-list",
//                 payLoad: res,
//             });
//         }, 1000);
//     });
//     console.log(action);
//     return action;
// }

// async function getListAction() {
//     let action = await axios.get("...").then((res) => {
//         return {
//             type: "change-list",
//             payLoad: res,
//         };
//     });
//     return action;
// }

// function getListAction() {
//     return axios.get("...").then((res) => {
//         return {
//             type: "change-list",
//             payLoad: res,
//         };
//     });
// }

export { getListAction };
