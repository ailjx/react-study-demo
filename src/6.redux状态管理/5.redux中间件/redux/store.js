import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";
import reduxPromise from "redux-promise";
import HomeReducer from "../reducers/TitleReducer";
import ListReducer from "../reducers/ListReducer";

const reducer = combineReducers({
    HomeReducer,
    ListReducer,
});

// redux开发者工具：https://github.com/zalmoxisus/redux-devtools-extension
// 使用composeEnhancers配合浏览器插件使用
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    reducer,
    // 使用applyMiddleware应用中间件
    composeEnhancers(applyMiddleware(reduxThunk, reduxPromise))
);
export default store;
