const HomeReducer = (
    prevState = {
        isShow: true,
    },
    action
) => {
    let newState = { ...prevState };
    switch (action.type) {
        case "hide":
            newState.isShow = false;
            return newState;
        case "show":
            newState.isShow = true;
            return newState;
        default:
            return prevState;
    }
};
export default HomeReducer;
