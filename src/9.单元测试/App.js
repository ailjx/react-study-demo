/*
 * @Author: AiLjx
 * @Date: 2022-06-27 21:48:59
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 22:42:50
 */
// 暂时无法使用！！！
import React, { useState, useRef } from "react";

export default function App() {
    const [list, setlist] = useState([1, 2, 3]);
    const RefInput = useRef(null);
    return (
        <div>
            <h1>React单元测试</h1>
            <input type='text' ref={RefInput} />
            <button onClick={() => setlist([RefInput.current.value, ...list])}>
                添加
            </button>
            <ul>
                {list.map((item) => (
                    <li key={item}>{item}</li>
                ))}
            </ul>
        </div>
    );
}
