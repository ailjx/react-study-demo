/*
 * @Author: AiLjx
 * @Date: 2022-06-27 21:53:20
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 22:41:53
 */
// 安装官方的单元测试：npm i react-test-renderer
// ShallowRender做浅层次渲染，ReactTestUtil做模拟点击
import ShallowRender from "react-test-renderer/shallow";
import App from "../App";
// import ReactTestUtil from "react-dom/test-utils";
describe("react-test-render", function () {
    it("app 的名字事kerwin-todo", function () {
        const render = new ShallowRender();
        render.render(<App />);
        // console.log(render.getRenderOutput().props.children[0].type)

        expect(render.getRenderOutput().props.children[0].type).toBe("h1");
        expect(render.getRenderOutput().props.children[0].props.children).toBe(
            "kerwin-todo"
        );
    });
});
