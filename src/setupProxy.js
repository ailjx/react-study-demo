/*
 * @Author: AiLjx
 * @Date: 2022-06-21 09:51:30
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 19:15:21
 */
// 每次修改setupProxy.js都需要手动重启服务器
// 反向代理，解决跨域
const { createProxyMiddleware } = require("http-proxy-middleware");
module.exports = function (app) {
    app.use(
        "/api", // 这里这个根据实际接口填写，一般请求下接口的开头都是固定的，将开头填到这里即可
        createProxyMiddleware({
            // 将本地服务器（http://localhost:3000）代理到http://localhost:5000，
            // 请求/api相当于请求http://localhost:5000/api
            // 请求配置之外的请求不受影响，如请求/list，实际还是请求http://localhost:3000/list
            target: "http://localhost:5000",
            // 改变域名，固定写法
            changeOrigin: true,
        })
    );
    // 多个代理
    app.use(
        "/ajax",
        createProxyMiddleware({
            target: "https://i.maoyan.com",
            changeOrigin: true,
        })
    );
    app.use(
        "/graphql",
        createProxyMiddleware({
            target: "http://localhost:3001",
            changeOrigin: true,
        })
    );
};
/**
 * 跨域出现原因：
 * 跨域是浏览器阻止的，存在于浏览器，当向外发送请求时浏览器发现域名不一样就会进行阻止，
 * 如从我们的http://localhost:3000直接向http://localhost:5000发请求就会被浏览器阻止；
 *
 * 服务器之间没有跨域问题，所以可以使用反向代理将本地服务器代理到外部服务器上（如http://localhost:5000）
 * 这样当我们访问本地服务器/api时（朝自己发请求），浏览器不会阻止，
 * 同时本地服务器的/api请求代理到了外部服务器帮我们向外请求了http://localhost:5000/api
 *
 * 整个流程是我们向本地服务器发请求，然后本地服务器根据代理代理到了外部服务器（服务器之间不存在跨域，可以直接代理）
 * 从而实现我们绕过浏览器的跨域阻止向外部服务器发送请求，本地服务器相当于一个中转站
 *  */
