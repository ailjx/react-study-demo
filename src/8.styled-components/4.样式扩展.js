/*
 * @Author: AiLjx
 * @Date: 2022-06-27 19:37:07
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 19:40:36
 */
import React from "react";
import styled from "styled-components";
export default function App() {
    return (
        <div>
            {/* 三者只有颜色不同 */}
            <StyleButton1></StyleButton1>
            <StyleButton2></StyleButton2>
            <StyleButton3></StyleButton3>
        </div>
    );
}

const StyleButton1 = styled.button`
    width: 60px;
    height: 30px;
    background: #333;
`;
// 根据StyleButton1进行样式扩展（继承StyleButton1）
const StyleButton2 = styled(StyleButton1)`
    // 样式会覆盖继承的同名样式
    background: red;
`;
const StyleButton3 = styled(StyleButton1)`
    background: yellow;
`;
