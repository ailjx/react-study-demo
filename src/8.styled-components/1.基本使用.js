/*
 * @Author: AiLjx
 * @Date: 2022-06-27 18:48:44
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 19:09:55
 */
// 使用style-component将css代码写入jsx中，html，css，js代码都在js中：all in js
// 安装：npm i styled-components
import React from "react";
import styled from "styled-components";
export default function App() {
    return (
        <div>
            App:
            {/* 使用styled创建的footer标签替代原来的footer */}
            <StyledFooter>
                {/* <footer> */}
                <ul>
                    <li>首页</li>
                    <li>搜索</li>
                    <li>列表</li>
                    <li>我的</li>
                </ul>
                {/* </footer> */}
            </StyledFooter>
        </div>
    );
}

// 基本语法：styled.标签名`css样式`
const StyledFooter = styled.footer`
    background: #333;
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    // 支持flex语法
    ul {
        display: flex;
        height: 60px;
        text-align: center;
        line-height: 60px;
        li {
            flex: 1;
            color: #fff;
            list-style: none;
            &:hover {
                background: pink;
            }
        }
    }
`;
