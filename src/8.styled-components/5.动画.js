/*
 * @Author: AiLjx
 * @Date: 2022-06-27 19:41:38
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 19:46:12
 */
import React from "react";
// 引入keyframes设置动画
import styled, { keyframes } from "styled-components";

export default function App() {
    return (
        <div>
            <StyledDiv></StyledDiv>
        </div>
    );
}
const myaniamtion = keyframes`
    from{
        transform:rotate(0deg)
    }
    to{
        transform:rotate(360deg)
    }
`;

const StyledDiv = styled.div`
    background: #333;
    width: 100px;
    height: 100px;
    // 引入动画名
    animation: ${myaniamtion} 1s infinite;
`;
