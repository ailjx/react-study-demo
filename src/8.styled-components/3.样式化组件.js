/*
 * @Author: AiLjx
 * @Date: 2022-06-27 19:30:31
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 19:35:53
 */
import React from "react";
import styled from "styled-components";
export default function App() {
    return (
        <div>
            App:
            <StyledDiv></StyledDiv>
        </div>
    );
}
// styled实际上也是一个高阶组件，它会将我们定义的css样式转换成类名，
// 然后通过props.className传给包裹的组件
const StyledDiv = styled(Child)`
    background: red;
`;
function Child(props) {
    // 需要被styled样式化的组件必须设置styled传来的className
    return <div className={props.className}>child</div>;
}
