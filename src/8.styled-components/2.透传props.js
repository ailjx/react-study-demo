/*
 * @Author: AiLjx
 * @Date: 2022-06-27 19:22:03
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 19:28:31
 */
import React from "react";
import styled from "styled-components";
export default function App() {
    return (
        <div>
            App:
            {/* 原生标签默认的属性会自动透传进入styled创建的组件，能在其上直接使用 */}
            <StyledInout type='password' placeholder='请输入'></StyledInout>
            {/* 自定义属性会自动通过props透传 */}
            <StyledDiv bg='red'></StyledDiv>
            <StyledDiv></StyledDiv>
        </div>
    );
}
const StyledInout = styled.input`
    outline: none;
    border-radius: 10px;
`;
const StyledDiv = styled.div`
    // 通过props获取传来的属性
    background: ${(props) => props.bg || "#333"};
    width: 100px;
    height: 100px;
`;
