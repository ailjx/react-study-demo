// 安装mobx: npm i mobx mobx-react-lite
//mobx-react-lite作为链接包，导出observer包裹组件（只能和函数组件配合）
//如果要和类组件配合，需要用另一个包：mobx-react
//引入定义好的counterStore
import { useStore } from "./store/index.js";
//引入更新视图的关键方法
import { observer } from "mobx-react-lite";

//路由配置
function App() {
  const rootStore = useStore();
  console.log(rootStore);
  //也可以通过解构将rootStore里的各个模块解构出来
  //注意：解构赋值到store实例对象就可以了，防止破坏响应式
  const { counterStore, listStore } = useStore();
  return (
    <div>
      App
      {/* 直接调用 */}
      <h1>mobx数据：{rootStore.counterStore.count}</h1>
      <h1>mobx计算属性：{rootStore.listStore.fillterList}</h1>
      {/* 通过解构后的store调用 */}
      <button onClick={() => counterStore.addCount()}>+1</button>
      <button onClick={() => listStore.addList()}>添加list</button>
    </div>
  );
}

export default observer(App);
