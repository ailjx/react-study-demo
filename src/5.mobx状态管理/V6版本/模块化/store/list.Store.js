import { computed, makeAutoObservable } from "mobx";
class ListStore {
  //定义数据
  list = [1, 2, 3, 4, 5, 6];
  constructor() {
    //响应式处理
    makeAutoObservable(this, {
      // 标记computed
      fillterList: computed,
    });
  }
  //get计算属性：computed,计算属性需要在makeAutoObservable里做一下标记
  get fillterList() {
    return this.list.filter((item) => item > 2);
  }
  addList = () => {
    this.list.push(6);
  };
}
//实例化
const listStore = new ListStore();
export default listStore;
