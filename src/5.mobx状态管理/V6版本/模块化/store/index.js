import React from "react";
import listStore from "./list.Store";
import counterStore from "./counter.Store";
class RootStore {
    // 组合store
    constructor() {
        //对子模块进行实例化操作并赋值给RootStore对应的属性
        //这样将来实例化RootStore的时候就可以通过对应的属性获取导入的对应子模块的实例对象
        this.listStore = listStore;
        this.counterStore = counterStore;
    }
}

// 实例化根store注入context
const rootStore = new RootStore();
// 使用React的useContext机制 导出useStore方法，供业务组件统一使用
// 之前知道context是通过Provider value={传递的数据}来传递数据
// 那createContext是什么？
// const MyContext = React.createContext(defaultValue);
// 创建一个 Context 对象（MyContext）。当 React 渲染一个订阅了这个 Context 对象（使用了MyContext）的组件，
// 这个组件会从组件树中离自身最近的那个匹配的 Provider 中读取到当前的 context 值（Provider value值）。
// 只有当组件所处的树中没有匹配到 Provider 时，其 defaultValue 参数才会生效。
// 此默认值有助于在不使用 Provider 包装组件的情况下对组件进行测试。

// useContext查找机制：优先从Provider value找，如果找不到，就会找createContext方法传递过来的默认参数
// 核心目的：让每个业务组件可以通过统一一样的方法获取store的数据
const context = React.createContext(rootStore);

// hooks useContext跨组件通信也用到了
// 通过useContext拿到rootStore实例对象，然后返回给useStore
// 导出useStore方法，供组件通过调用该方法使用根实例
// 在业务组件中 调用useStore()->rootStore
const useStore = () => React.useContext(context);
export { useStore };

//以上是模板代码，在不同项目都通用
