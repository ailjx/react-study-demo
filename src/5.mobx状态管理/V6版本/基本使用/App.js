//请注意运行V6版本案例时务必重新安装一下mobx和mobx-react: npm i mobx mobx-react-lite

//引入定义好的counterStore
import counterStore from "./store/counter";
//引入更新视图的关键方法
// mobx-react-lite轻量包，mobx-react 是他的大兄弟，它里面也引用了 mobx-react-lite 包。
// mobx-react提供了很多在新项目中不再需要的特性， mobx-react附加的特性有：
// 1. 对于React class components的支持。
// 2. Provider 和inject. MobX的这些东西在有 React.createContext 替代后变得不必要了。
// 3. 特殊的观察对象 propTypes。
// 要注意 mobx-react 是全量包，也会暴露 mobx-react-lite包中的任何方法,其中包含对函数组件的支持。
// 如果你使用 mobx-react，那就不要添加 mobx-react-lite 的依赖和引用了。
import { observer } from "mobx-react-lite";
//路由配置
function App() {
    return (
        <div>
            App
            <h1>mobx数据：{counterStore.count}</h1>
            <h1>mobx计算属性：{counterStore.fillterList.join("-")}</h1>
            <button onClick={() => counterStore.addCount()}>+1</button>
            <button onClick={() => counterStore.addList()}>添加list</button>
        </div>
    );
}

export default observer(App);
