// 在版本6之前，Mobx鼓励使用ES.next中的decorators（装饰器）,将某个对象标记为observable, computed 和 action。
// 然而，装饰器语法尚未定案以及未被纳入ES标准，标准化的过程还需要很长时间，
// 且未来制定的标准可能与当前的装饰器实现方案有所不同。出于兼容性的考虑，
// MobX 6中放弃了它们，并建议使用makeObservable / makeAutoObservable代替。

import { computed, makeAutoObservable } from "mobx";
class CounterStore {
    //1. 定义数据
    count = 0;
    list = [1, 2, 3, 4, 5, 6];
    constructor() {
        //2. 响应式处理
        // makeAutoObservable(this); //没有计算属性只需这样写
        makeAutoObservable(this, {
            // 标记computed
            fillterList: computed,
        });
    }
    //4. get计算属性：computed,计算属性需要在makeAutoObservable里做一下标记
    get fillterList() {
        return this.list.filter((item) => item > 2);
    }
    //3. 定义action函数
    addCount = () => {
        this.count++;
    };
    addList = () => {
        this.list.push(6);
    };
}
//实例化
const counterStore = new CounterStore();
export default counterStore;
