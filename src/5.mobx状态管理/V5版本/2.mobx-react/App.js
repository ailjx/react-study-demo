// 当前使用的是mobx5，则mobx-react也需要安装版本5，安装：npm i mobx-react@5
// 前面使用mobx时需要自己手动autorun订阅和取消订阅，比较麻烦
// 使用mobx-react就不需要自己手动autorun了

import React, { useState, useEffect } from "react";
import Home from "./Home";

// 类组件使用mobx-react时不需要外部导入store，它是从根组件Provider的props中获取
// import store from "./mobx/store";
// import { autorun } from "mobx"; // 不再需要手动autorun
import { inject, observer } from "mobx-react";

// 类组件使用mobx-react：
// 类组件使用mobx-react前确保index.js入口文件是：（函数式组件使用mobx-react时不需要这样操作）
// import { Provider } from "mobx-react";
// import store from "./5.mobx状态管理/V5版本/2.mobx-react/mobx/store";
// <Provider store={store}>
//      <App />
// </Provider>

// 使用装饰器
// @inject("key") 向组件props注入store对象，这里的key要与index.js入口文件中包裹App组件的Provider传的props名称对应
// @observer类似于高阶组件，帮组件自动订阅和取消订阅
@inject("store")
@observer
class App extends React.Component {
    render() {
        return (
            <div>
                {this.props.store.isShow && <h1>App标题</h1>}

                <button onClick={() => this.props.store.changeHome()}>
                    切换下方按钮
                </button>
                <hr />
                {this.props.store.home && <Home></Home>}
            </div>
        );
    }
}
export default App;

// 函数式组件使用mobx-react见Home组件
