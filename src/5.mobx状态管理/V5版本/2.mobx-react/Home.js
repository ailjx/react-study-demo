import React from "react";
// 函数式组件使用mobx-react时，不需要App根组件外包裹Provider，但需要自己引入store
import store from "./mobx/store";
// import { autorun } from "mobx"; // 不再需要手动autorun
import { observer, Observer } from "mobx-react";

// 方法一：使用observer直接包裹组件，observer作为高阶组件，吸收低级组件，吐出高级组件（将低级组件变得更强大）
// function Home() {
//     // 使用mobx-react后就不再需要自己设置状态和订阅、取消订阅了
//     // const [list, setlist] = useState(store.list);
//     // useEffect(() => {
//     // console.log("Home");
//     // 在组件初始化时开始订阅追踪状态，当组件被销毁重建时又会开始一个新的订阅
//     // 这会导致重复订阅问题，所以需要在组件销毁时取消订阅
//     // autorun返回一个能够取消订阅的函数
//     // const unsubscribe = autorun(() => {
//     //     // autorun第一次被创建和store状态list改变时执行
//     //     console.log("Home autorun");
//     //     setlist(store.list);
//     // });
//     // return () => {
//     //     // 取消订阅
//     //     unsubscribe();
//     // };
//     // }, []);

//     const getList = () => {
//         if (store.list.length === 0) {
//             store.getList();
//         }
//     };
//     console.log(999999999); // 获取列表后会再次打印
//     return (
//         <div>
//             <button onClick={() => store.changeHide()}>隐藏标题</button>
//             <button onClick={() => store.changeShow()}>显示标题</button>
//             <br />
//             <button onClick={getList}>获取列表</button>
//             <ul>
//                 {store.list.map((item) => (
//                     <li key={item}>{item}</li>
//                 ))}
//             </ul>
//         </div>
//     );
// }
// export default observer(Home);

// 方法二：使用Observer组件，细粒度更新
function Home() {
    const getList = () => {
        if (store.list.length === 0) {
            store.getList();
        }
    };
    console.log(999999999); // 获取列表后不会再次打印
    return (
        <div>
            <button onClick={() => store.changeHide()}>隐藏标题</button>
            <button onClick={() => store.changeShow()}>显示标题</button>
            <br />
            <button onClick={getList}>获取列表</button>
            {/* Observer会监听内部使用的store状态，它什么时候更新，Observer就会执行内部的函数return页面更新视图 */}
            <Observer>
                {() => {
                    return (
                        <>
                            <ul>
                                {store.list.map((item) => (
                                    <li key={item}>{item}</li>
                                ))}
                            </ul>
                        </>
                    );
                }}
            </Observer>
        </div>
    );
}
export default Home;

// observer是直接包裹整个组件，一个store状态变化整个组件都会重新渲染
// 而Observer可以选择性的包裹使用store状态的视图，来实现细粒度的更新，避免未变化的视图重复渲染
