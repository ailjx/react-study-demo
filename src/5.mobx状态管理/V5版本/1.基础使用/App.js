// mobx和react职责划分：
// mobxStore：1.业务状态数据（作全局状态管理） 2.业务状态操作逻辑
// React：1.渲染业务数据 2.UI临时状态维护 3.事件触发，调用Mobx
// 安装V5版本：npm i mobx@5
import React, { useState, useEffect } from "react";
import Home from "./Home";
// 引入store：Mobx全局状态厂库
import store from "./mobx/store";
import { autorun } from "mobx";

export default function App() {
    const [isShow, setisShow] = useState(store.isShow);

    const [home, sethome] = useState(store.home);
    useEffect(() => {
        // 在组件初始化时开始追踪状态
        autorun(() => {
            // autorun第一次被创建和store状态isShow或home改变时执行
            console.log("App autorun");
            setisShow(store.isShow);
            sethome(store.home);
        });
    }, []);

    return (
        <div>
            {isShow && <h1>App标题</h1>}

            <button onClick={() => store.changeHome()}>切换下方按钮</button>
            <hr />
            {home && <Home></Home>}
        </div>
    );
}
