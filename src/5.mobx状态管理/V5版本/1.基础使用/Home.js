import React, { useState, useEffect } from "react";
import store from "./mobx/store";
import { autorun } from "mobx";
export default function Home() {
    const [list, setlist] = useState(store.list);

    useEffect(() => {
        console.log("Home");
        // 在组件初始化时开始订阅追踪状态，当组件被销毁重建时又会开始一个新的订阅
        // 这会导致重复订阅问题，所以需要在组件销毁时取消订阅
        // autorun返回一个能够取消订阅的函数
        const unsubscribe = autorun(() => {
            // autorun第一次被创建和store状态list改变时执行
            console.log("Home autorun");
            setlist(store.list);
        });
        return () => {
            // 取消订阅
            unsubscribe();
        };
    }, []);

    const hide = () => {
        // 直接在视图中修改store状态是不提倡的，提倡逻辑和UI视图分开
        // 在启用严格模式enforceActions时，这种在视图中直接修改store会报错
        // store.isShow = false;

        // 正确方式应该是调用store中的方法，让store自己对状态修改
        store.changeHide();
    };

    const show = () => {
        // store.isShow = true;

        store.changeShow();
    };

    const getList = () => {
        if (store.list.length === 0) {
            store.getList();
        }
    };

    return (
        <div>
            <button onClick={hide}>隐藏标题</button>
            <button onClick={show}>显示标题</button>
            <br />
            <button onClick={getList}>获取列表</button>
            <ul>
                {list.map((item) => (
                    <li key={item}>{item}</li>
                ))}
            </ul>
        </div>
    );
}
