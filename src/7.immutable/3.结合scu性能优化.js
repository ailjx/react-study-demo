import React, { Component } from "react";
import { Map } from "immutable";
export default class App extends Component {
    state = {
        love: Map({
            age: 18,
            filter: Map({
                name: "小明",
            }),
        }),
    };
    render() {
        return (
            <div>
                App
                <button
                    onClick={() => {
                        this.setState({
                            love: this.state.love.set("age", 20),
                        });
                    }}>
                    修改age
                </button>
                age:{this.state.love.get("age")}
                {/*类组件的问题： 修改age，导致state状态改变，App重新渲染
                即使Child的props没有改变Child也会被重新渲染，解决办法：使用scu生命周期阻止重复渲染 */}
                <Child filter={this.state.love.get("filter")}></Child>
            </div>
        );
    }
}
class Child extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        // 没用immutable之前是 if (JSON.stringify(this.props.filter) === JSON.stringify(nextProps.filter)) 判断
        // 使用JSON判断缺陷是不能有undefined，这样做不够完美

        // 而使用了immutable，filter是immutable类型的，可以直接判断前后是否直接相等，这样就完美了
        if (this.props.filter === nextProps.filter) {
            // 阻止重新渲染
            return false;
        }
        return true;
    }
    componentDidUpdate() {
        console.log("重新渲染了~~~", this.props.filter);
    }
    render() {
        return <div>Child</div>;
    }
}
