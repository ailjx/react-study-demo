/**
 * 我们知道react的设计原则是数据不可变，每次我们需要修改状态时都需要先深拷贝一份（redux也是），
 * 修改拷贝的那一份然后再将其重新赋值给状态，我们之前实现深拷贝的方法大多是...扩展运算符，
 * 但它具有一定的弊端，在介绍Immutable之前，我们先了解一下数据拷贝的几种方式
 */

// 1.引用复制（浅拷贝）
// let obj1 = {
//     age: 18,
// };
// let obj2 = obj1;
// obj2.age = 20;
// console.log(obj1, obj2); // 修改obj2，obj也会被修改

// 2.ES6扩展运算符：...（只能深拷贝第一层）
// let obj1 = {
//     age: 18,
//     a: {
//         b: 18,
//     },
// };
// let obj2 = { ...obj1 };
// obj2.age = 20;
// obj2.a.b = 20;
// // 修改obj2.age，obj1.age不会被修改（第一层是深拷贝）
// // 修改obj2.a.b，obj1.a.b会被修改（其它层是浅拷贝）
// console.log(obj1, obj2);

// 3.JSON.parse()配合JSON.stringify() （深拷贝，缺点：不能出现undefined，JSON不能转换undefined值）
// let obj1 = {
//     age: {
//         is: 18,
//     },
//     name: undefined,
// };
// let obj2 = JSON.parse(JSON.stringify(obj1));
// // obj2会丢失值为undefined的name属性，所以严格意义上当出现undefined时就算不上是拷贝了
// obj2.age.is = 20;
// console.log(obj1, obj2); // 无论怎么修改obj2，obj1都不被影响

// 4.deepcopy
// 递归深复制：一层层复制，性能不好，占用内容

// 研究过上述方法后，会发现对于安全可靠的深拷贝react状态我们自己好像没有什么好的方法了，
// 这就需要引入第三方库：immutable：npm i immutable

export default function App() {
    return (
        <div>
            <h1>immutable介绍：</h1>
            <h4>immutable介绍：https://github.com/immutable-js/immutable-js</h4>
            <p>
                每次修改一个
                Immutable对象时都会创建一个新的不可变的对象，在新对象上操作并不会影响到原对象的数据。
            </p>
            <h4>Immutable性能优化方式：</h4>
            <p>
                Immutable 实现的原理是 Persistent Data
                Structure（持久化数据结构），也就是使用旧数据创建新数据时，要
                保证旧数据同时可用且不变。同时为了避免 deepCopy
                把所有节点都复制一遍带来的性能损耗，Immutable 使用 了 Structural
                Sharing（结构共享），即如果对象树中一个节点发生变化，只修改这个节点和受它影响的父节点，
                其它节点则进行共享。
            </p>
        </div>
    );
}
