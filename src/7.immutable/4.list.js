// 前面说到了使用map转换对象，这里我们将使用list转换数组
import React, { useState } from "react";
import { List, fromJS } from "immutable";
// List使用方式和Map类似：将普通JS数组转换为immutable List对象
let arr = List([1, 2, 3]);
// 这里的push不是数组的push而是List对象上的方法，它不会改变arr，而是返回一个新的值，也印证了immutable数据不可变
// 数组上的方法在转换后的List对象上都可以直接使用，不同的是这些List上的方法都会返回一个修改后的新数组
// 像shift这种本来就具有返回值的方法（返回第一个元素的值），在List对象上使用返回的还是新数组
let arr2 = arr.push(4);
let arr3 = arr2.concat([5, 6, 7]); // 数组连接
let is = arr2.shift(); // 把数组的第一个元素从其中删除，并返回第一个元素的值

console.log("is----->", is.toJS()); // 返回的还是修改后的新数组，而不是第一个元素的值
console.log(arr.toJS(), arr2.toJS(), arr3.toJS());

// 在前面我们用到了fromJS来转换复杂对象，它对含有数组的复杂数据同样有效
let obj = fromJS({
    obj2: {
        list: [1, 2, 3],
    },
});
// 需要将list中的2删除，我们目前最简便的方法是这样做：
// let newObj = obj.setIn(
//     ["obj2", "list"],
//     obj.getIn(["obj2", "list"]).splice(1, 1)
// );
// 这样还是有点麻烦，让我们看一下updateIn的使用：
// 第一个参数数组是需要更新数据的对象，第二个参数是一个具有原数据参数的函数，该函数返回值即是新值
let newObj = obj.updateIn(["obj2", "list"], (list) => list.splice(1, 1));
console.log(newObj.toJS().obj2.list); // [1,3]

export default function App() {
    const [list] = useState(List([1, 2, 3, 4, 5, 6, 7]));
    return (
        <div>
            App
            <ul>
                {/* 在immutable List对象直接使用map也是可以的 */}
                {list.map((item) => (
                    <li key={item}>{item}</li>
                ))}
            </ul>
        </div>
    );
}
