import React, { useState } from "react";
import { Map, List, fromJS } from "immutable";

// 使用方式一：笨方法
export default function App() {
    // 手动Map，List转换原数据
    const orgData = Map({
        name: "小明",
        loca: Map({
            pro: "浙江",
            city: "杭州",
        }),
        favor: List(["唱跳", "篮球", "rap"]),
    });
    const [data, setdata] = useState({ info: orgData });
    return (
        <div>
            <h1>使用方法一：最笨方法，Map，List，get，set</h1>
            name:{data.info.get("name")}
            <br />
            地点：{data.info.get("loca").get("pro")}-
            {data.info.get("loca").get("city")}
            <button
                onClick={() => {
                    let newInfo = data.info
                        .set("name", "小红")
                        .set("loca", data.info.get("loca").set("pro", "北京"));

                    setdata({ info: newInfo });
                }}>
                修改
            </button>
            <br />
            爱好：
            <ul>
                {data.info.get("favor").map((item, index) => (
                    <li key={index}>
                        {item}
                        <button
                            onClick={() => {
                                let newinfo = data.info.set(
                                    "favor",
                                    data.info.get("favor").splice(index, 1)
                                );
                                setdata({ info: newinfo });
                            }}>
                            删除
                        </button>
                        <button
                            onClick={() => {
                                let newinfo = data.info.set(
                                    "favor",
                                    // 使用set修改数组内元素，将key指定为元素对应下标即可
                                    // 因为数据本质是个以下标为key的对象
                                    data.info.get("favor").set(index, "修改啦")
                                );
                                setdata({ info: newinfo });
                            }}>
                            修改
                        </button>
                    </li>
                ))}
            </ul>
            <hr />
            <App2></App2>
            <hr />
            <App3></App3>
        </div>
    );
}
// 使用方式二：一般方法
function App2() {
    // 原数据不变
    const orgData = {
        name: "小明",
        loca: {
            pro: "浙江",
            city: "杭州",
        },
        favor: ["唱跳", "篮球", "rap"],
    };
    // 使用fromJS转换后的数据作为状态
    const [data, setdata] = useState({ info: fromJS(orgData) });
    return (
        <div>
            <h1>使用方法二：一般方法，fromJS，getIn，setIn，updateIn</h1>
            name:{data.info.get("name")}
            <br />
            地点：{data.info.getIn(["loca", "pro"])}-
            {data.info.getIn(["loca", "city"])}
            <button
                onClick={() => {
                    let newInfo = data.info
                        .set("name", "小红")
                        .setIn(["loca", "pro"], "北京");

                    setdata({ info: newInfo });
                }}>
                修改
            </button>
            <br />
            爱好：
            <ul>
                {data.info.get("favor").map((item, index) => (
                    <li key={index}>
                        {item}
                        <button
                            onClick={() => {
                                let newinfo = data.info.updateIn(
                                    ["favor"],
                                    (list) => list.splice(index, 1)
                                );
                                setdata({ info: newinfo });
                            }}>
                            删除
                        </button>
                        <button
                            onClick={() => {
                                let newinfo = data.info.setIn(
                                    ["favor", index],
                                    "修改啦"
                                );
                                setdata({ info: newinfo });
                            }}>
                            修改
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
}
// 使用方式三：仅使用fromJS和toJS进行深拷贝
function App3() {
    // 原数据不变
    const orgData = {
        name: "小明",
        loca: {
            pro: "浙江",
            city: "杭州",
        },
        favor: ["唱跳", "篮球", "rap"],
    };
    // 使用原数据作为状态
    const [data, setdata] = useState({ info: orgData });
    return (
        <div>
            <h1>使用方法三：仅使用fromJS和toJS进行深拷贝</h1>
            name:{data.info.name}
            <br />
            地点：{data.info.loca.pro}-{data.info.loca.city}
            <button
                onClick={() => {
                    // 深拷贝
                    let newInfo = fromJS(data.info).toJS();
                    newInfo.name = "小红";
                    newInfo.loca.pro = "北京";
                    setdata({ info: newInfo });
                }}>
                修改
            </button>
            <br />
            爱好：
            <ul>
                {data.info.favor.map((item, index) => (
                    <li key={index}>
                        {item}
                        <button
                            onClick={() => {
                                let newInfo = fromJS(data.info).toJS();
                                newInfo.favor.splice(index, 1);
                                setdata({ info: newInfo });
                            }}>
                            删除
                        </button>
                        <button
                            onClick={() => {
                                let newInfo = fromJS(data.info).toJS();
                                newInfo.favor[index] = "修改啦";
                                setdata({ info: newInfo });
                            }}>
                            修改
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
}
