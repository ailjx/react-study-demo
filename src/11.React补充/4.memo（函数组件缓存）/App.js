/*
 * @Author: AiLjx
 * @Date: 2022-06-28 20:05:48
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 20:13:32
 */
// 利用memo做性能优化：
// PureComponent 只能用于class 组件，memo 用于functional 组件
import { memo, useState } from "react";

export default function App() {
    const [count, setcount] = useState(0);
    return (
        <div>
            <button onClick={() => setcount(count + 1)}>+1</button>
            <Child1></Child1>
            <Child2></Child2>
        </div>
    );
}
function Child1() {
    console.log("child1渲染了"); // +1点一次，log一次
    return <div>child1</div>;
}
const Child2 = memo(() => {
    console.log("child2渲染了"); // 仅在第一次渲染时log
    return <div>child1</div>;
});

// 父组件状态改变时会连同子组件重新渲染，但使用memo可缓存组件避免重复渲染
// 当传递的props未发生改变时，memo包裹的组件不会重新渲染

// 性能优化：shouldComponentUpdate生命周期，PureComponent和memo组件
