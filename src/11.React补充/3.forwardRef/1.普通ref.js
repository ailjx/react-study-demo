/*
 * @Author: AiLjx
 * @Date: 2022-06-28 19:45:03
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 19:57:53
 */
// 案例效果：点击父组件按钮，操作子组件内input框
// 普通方法：
import { useEffect, useRef } from "react";

export default function App() {
    let input = null;
    return (
        <div>
            <button
                onClick={() => {
                    input.value = "";
                    input.focus();
                }}>
                获取焦点并清空输入框
            </button>
            <Child
                cb={(el) => {
                    console.log(el.current);
                    input = el.current;
                }}></Child>
        </div>
    );
}

function Child({ cb }) {
    const inputRef = useRef(null);
    useEffect(() => {
        // DOM渲染后通过props向父组件传递ref对象
        cb(inputRef);
    }, [cb]);

    return (
        <div style={{ background: "red" }}>
            <input type='text' ref={inputRef} defaultValue='99999999' />
        </div>
    );
}
