/*
 * @Author: AiLjx
 * @Date: 2022-06-28 19:53:21
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 19:58:08
 */
// 案例效果：点击父组件按钮，操作子组件内input框
// forwardRef方法：
import { useRef, forwardRef } from "react";

export default function App() {
    const inputRef = useRef(null);
    return (
        <div>
            <button
                onClick={() => {
                    inputRef.current.value = "";
                    inputRef.current.focus();
                }}>
                获取焦点并清空输入框
            </button>
            {/* 直接对组件进行ref */}
            <Child ref={inputRef}></Child>
        </div>
    );
}

// 使用forwardRef透传ref，forwardRef包裹函数组件，
// 第一个参数是props，第二个参数是组件的ref
const Child = forwardRef((props, ref) => {
    return (
        <div style={{ background: "red" }}>
            <input type='text' ref={ref} defaultValue='99999999' />
        </div>
    );
});

// forwardRef不能透传多个ref
