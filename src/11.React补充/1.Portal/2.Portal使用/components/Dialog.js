/*
 * @Author: AiLjx
 * @Date: 2022-06-28 16:11:35
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 16:54:59
 */
import React from "react";
import { createPortal } from "react-dom";
export default function Dialog({ onClose }) {
    // 使用createPortal将节点渲染到第二个参数的位置里
    return createPortal(
        <div
            style={{
                position: "fixed",
                width: "100%",
                height: "100%",
                background: "rgba(0,0,0,0.7)",
                top: "0",
                left: "0",
                color: "#fff",
                zIndex: "999999999",
            }}>
            Dialog
            <button onClick={onClose}>关闭</button>
        </div>,
        document.body
    );
}
// 这里就可以看到我们将Dialog内的所有节点利用createPortal渲染到了body标签内
// 使其不会再受父节点.right的样式影响

// 虽然通过portal渲染的元素在父组件的盒子之外，但是渲染的dom节点仍在React的元素树上，
// 在那个dom元素上的点击事件仍然能在dom树中监听到。
// 如点击关闭按钮后，点击事件任然会按照原来的dom树进行冒泡，从而触发class为App的div元素
// 上的点击事件，打印出：App点击事件触发
