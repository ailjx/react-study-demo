/*
 * @Author: AiLjx
 * @Date: 2022-06-28 16:11:16
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 16:51:59
 */

import React from "react";
import { useState } from "react";
import "./App.css";
import Dialog from "./components/Dialog";
export default function App() {
    const [isShow, setisShow] = useState(false);
    return (
        <div
            className='App'
            onClick={() => {
                console.log("App点击事件触发");
            }}>
            <div className='left'></div>
            <div className='right'>
                <button onClick={() => setisShow(true)}> 打开弹窗</button>
                {isShow && <Dialog onClose={() => setisShow(false)}></Dialog>}
            </div>
        </div>
    );
}
