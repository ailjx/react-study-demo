/*
 * @Author: AiLjx
 * @Date: 2022-06-28 16:11:16
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 16:55:28
 */

// Portal 提供了一种将子节点渲染到指定DOM节点中的方式，该节点存在于 DOM 组件的层次结构之外。
// Portal像是一个传送门，能把组件内节点渲染到指定DOM结构内，使子组件不仅仅只能在父组件DOM结构内渲染
// 常用的地方有弹出框，对话框等，将弹出框组件节点利用Portal渲染到根节点之外，
// （成为body节点的孩子，与root节点同级），这样做的好处是可以避免一些组件的定位和显示问题
// 接下来将复现这些问题：
// 这样一个场景，我们点击一个按钮，展示一个弹窗，弹窗具有一个全屏的半透明阴影遮罩，
// 我们使用普通的方法如下：
import React from "react";
import { useState } from "react";
import "./App.css";
import Dialog from "./components/Dialog";
export default function App() {
    const [isShow, setisShow] = useState(false);
    return (
        <div className='App'>
            <div className='left'></div>
            <div className='right'>
                {/* 点击按钮查看弹窗效果，问题出现，解释见Dialog.js */}
                <button onClick={() => setisShow(true)}> 打开弹窗</button>
                {isShow && <Dialog></Dialog>}
            </div>
        </div>
    );
}
