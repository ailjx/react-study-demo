/*
 * @Author: AiLjx
 * @Date: 2022-06-28 16:11:35
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 16:53:54
 */
import React from "react";

export default function Dialog() {
    return (
        <div
            style={{
                position: "fixed",
                width: "100%",
                height: "100%",
                background: "rgba(0,0,0,0.7)",
                top: "0",
                left: "0",
                color: "#fff",
                zIndex: "999999999",
            }}>
            Dialog
        </div>
    );
}

// 我们给弹窗设置了非常大的zIndex优先值，利用固定定位实现宽高都是100%
// 但当我们显示弹窗组件时，发现显示出的效果中宽并不是100%，为什么呢？
// 因为此时Dialog组件渲染到了是class为right的div内部，而class为right的zIndex小于class为left的
// 所以无论Dialog组件的zIndex优先值有多大，它都会受限于父节点而无法遮盖住left，
// 为了避免这种问题，我们就可以使用Portal将Dialog组件内的节点渲染到根节点之外，
// 使它不再受限于父节点
