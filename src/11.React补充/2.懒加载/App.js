/*
 * @Author: AiLjx
 * @Date: 2022-06-28 19:25:47
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 19:35:38
 */

// 懒加载实现原理：当 Webpack 解析到该语法时，它会自动地开始进行代码分割(Code Splitting)，
// 分割成一个文件，当使用到这个文件的时候会这段代码才会被异步加载。
// 在 React.lazy 和常用的三方包 react-loadable ，都是使用了这个原理，
// 然后配合webpack进行代码打包拆分达到异步加载，这样首屏渲染的速度将大大的提高。
// 由于 React.lazy 不支持服务端渲染，所以这时候 react-loadable 就是不错的选择。

// 使用lazy和Suspense实现组件懒加载
import { useState, lazy, Suspense } from "react";
// import Home from "./components/Home";
// import About from "./components/About";

const Home = lazy(() => import("./components/Home"));
const About = lazy(() => import("./components/About"));
export default function App() {
    const [is, setis] = useState(1);
    return (
        <div>
            <button onClick={() => setis(1)}>首页</button>
            <button onClick={() => setis(0)}>关于</button>
            {/* fallback占位内容 */}
            <Suspense fallback={<h1>正在加载..</h1>}>
                {is ? <Home></Home> : <About></About>}
            </Suspense>
        </div>
    );
}
