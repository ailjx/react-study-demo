import React, { useState, useCallback } from "react";
/**
 * useCallback
 * 防止因为组件重新渲染，导致方法被重新创建，起到缓存作用，提高性能；
 * 只有第二个参数变化了，才重新声明一次
 */
export default function App() {
    const [count, setcount] = useState(0);
    /**
     * useCallback不会执行第一个参数函数，而是将它返回给你
     */
    const change = useCallback(() => {
        setcount(count + 1);
    }, [count]);
    /**
     * 只有count改变后，这个函数才会重新声明，其它状态无论怎么改变都不会导致该函数重新创建，从而提高性能
     * 如果第二个参数传入空数组即无依赖项，那么在第一次创建后就被缓存，count后期改变时，该函数里拿到的还是老的count，就起不到count一直+1的效果了
     * 如果不传第二个参数，每次都会重新声明一次，则useCallback就没啥意义了
     */
    return (
        <div>
            <button
                onClick={() => {
                    change();
                }}>
                add
            </button>
            {count}
        </div>
    );
}
