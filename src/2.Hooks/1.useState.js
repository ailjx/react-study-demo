//hooks为函数组件提供了状态
//hooks只能在函数组件中使用
//hooks：一套能够使函数组件更强大，更灵活的“钩子”
//某种意义上，hooks的出现 就是想不用生命周期概念也可以写业务代码

// useState注意事项
// 1. 只能出现在函数组件中
// 2. 不能嵌套在if/for/其它函数中（react按照hooks的调用顺序识别每一个hook）
import { useState } from "react";

function App() {
    // 参数：状态初始值比如,传入 0 表示该状态的初始值为 0
    // 返回值：数组,包含两个值：1. 状态值（state） 2. 修改该状态的函数（setState）
    // 这里采用数组解构赋值的方式获取状态值和修改状态的函数，所以count, setCount为自己自定义的，建议自定义时保持语义化
    const [count, setCount] = useState(0);
    // useState 函数可以执行多次，每次执行互相独立，每调用一次为函数组件提供一个状态
    const [list, setList] = useState([1, 2, 3]);
    const setData = () => {
        setCount(count + 1);
        setList(list.filter((item) => item !== 2));
    };
    //每次状态修改整个函数组件都会重新执行一次，但是每次重新执行时状态都是最新的，
    //与this.setState不同的是，如果修改前后状态相同则不会触发组件更新
    //useState 的初始值(参数)只会在组件第一次渲染时生效。
    //也就是说，以后的每次渲染，useState 获取到都是最新的状态值，React 组件会记住每次最新的状态值
    console.log("状态修改了", count, list);

    return (
        <div>
            <button onClick={setData}>修改状态</button>
            <hr />
            <span>{count}</span>
            <ul>
                {list.map((item) => (
                    <li key={item}>{item}</li>
                ))}
            </ul>
            <hr />
            <Counter name={9}></Counter>
            <Counter name={99}></Counter>
        </div>
    );
}
function Counter({ name }) {
    // useState - 回调函数的参数
    //参数只会在组件的初始渲染中起作用，后续渲染时会被忽略。如果初始 state 需要通过计算才能获得，则可以传入一个函数，在函数中计算并返回初始的 state，此函数只在初始渲染时被调用
    const [myname, setMyname] = useState(() => name + 1);
    return (
        <div>
            <button onClick={() => setMyname(myname + 1)}>修改{myname}</button>
        </div>
    );
}
export default App;
