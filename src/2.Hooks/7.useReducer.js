/**
 * useReducer是react hooks提供的全局状态管理方案，类似于redux
 * useReducer接收三个参数：1.状态处理函数（函数返回值作为状态的新值） 2.外部状态对象 3.惰性初始化：见https://zh-hans.reactjs.org/docs/hooks-reference.html#lazy-initialization
 * useReducer返回当前的 state 以及与其配套的 dispatch 方法(改变状态的唯一方法)。
 * 在复杂通信的场景下useReducer常和useConText配合使用，见小案例/useReducer使用
 */

import React, { useReducer } from "react";

// 外部状态对象
const initialState = {
    count: 0,
    // list:[]
};

//处理函数
const reducer = (prevState, action) => {
    //不能直接修改原状态，需要先深copy一份
    let nextState = { ...prevState };
    switch (action.type) {
        case "decrement":
            nextState.count--;
            return nextState; //函数返回值即是新的状态
        case "increment":
            nextState.count++;
            return nextState;
        default:
            return nextState;
    }
};

export default function App() {
    const [state, dispatch] = useReducer(reducer, initialState);
    return (
        <>
            <button onClick={() => dispatch({ type: "decrement" })}>-</button>
            Count: {state.count}
            <button onClick={() => dispatch({ type: "increment" })}>+</button>
        </>
    );
}
