/**
 * 约定自定义hooks必须使用use开头，不然React将无法检查你的Hook是否违反了Hook的规则
 */

import { useState } from "react";
// 实现获取滚动距离Y
export default function useWindowScroll() {
    const [y, setY] = useState(0);
    window.addEventListener("scroll", () => {
        const h = document.documentElement.scrollTop;
        setY(h);
    });
    return [y];
}
