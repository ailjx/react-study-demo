import useWindowScroll from "./useWindowScroll";
import useLocalStorage from "./useLocalStorage";
function App() {
    const [y] = useWindowScroll();
    const [message, setMessage] = useLocalStorage("hook-key", "阿美");

    function setData() {
        setMessage("阿帅");
    }
    return (
        <div style={{ height: "400vh" }}>
            <div style={{ position: "sticky", top: "0px", marginTop: "100px" }}>
                {y}--{message}
                <button onClick={setData}>修改message</button>
            </div>
        </div>
    );
}

export default App;

/**
 * 使用自定义Hooks的精髓在于提高组件复用性，使代码结构更加清晰，通过解耦合提高代码维护性
 */
