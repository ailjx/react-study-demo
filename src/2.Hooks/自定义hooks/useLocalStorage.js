// 可以自动同步到本地LocalStorage
import { useState, useEffect } from "react";
// 传入defaultValue作为message的默认初始值
export default function useLocalStorage(key, defaultValue) {
  const [message, setMessage] = useState(defaultValue);
  useEffect(() => {
    // 每次只要message变化 就会自动同步到本地
    window.localStorage.setItem(key, message);
  }, [message, key]);
  return [message, setMessage];
}
