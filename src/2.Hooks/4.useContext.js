import React, { createContext, useContext, useState } from "react";
const context = createContext();
//组件结构：App-->SonA-->SonAsonB
//这里是将App的数据通过跨组件通信直接传给SonAsonB
//通过hooks：useContext来进行跨组件通信，与9.跨组件通信.js做对比

//如果Context需要传递的数据，只需要在整个应用初始化的时候传递一次就可以，即在index.js文件对App组件用Provider包裹
const SonA = () => {
    // 底层组件通过useContext函数获取数据
    const msg = useContext(context);
    console.log(msg);
    return (
        <>
            <div>
                我是App的子组件SonA组件：
                <br />
                我接收的数据：
                <h1> {msg}</h1>
            </div>
            <hr />
            <SonAsonB></SonAsonB>
        </>
    );
};
const SonAsonB = () => {
    const msg = useContext(context);
    return (
        <div>
            我是SonA的子组件：SonAsonB
            <br />
            我接收到的数据： <h1> {msg}</h1>
        </div>
    );
};
//父组件App
function App() {
    const [msg, setMsg] = useState(0);
    return (
        // 顶层组件通过Provider 提供数据
        <context.Provider value={msg}>
            <div className='App'>
                我是App组件：
                <button onClick={() => setMsg(msg + 1)}>+1</button>
                我向下传的数据{msg}
                <hr />
                <SonA></SonA>
            </div>
        </context.Provider>
    );
}

export default App;
