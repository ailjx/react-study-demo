import React, { useState, useMemo, useCallback } from "react";
/**
 * useCallback的功能完全可以由useMemo所取代，useMemo也可以当作《计算属性》而存在
 * useCallback不会执行第一个参数函数，而是将它返回给你，而useMemo会执行第一个函数并且将函数执行结果返回给你
 * 即：useCallback(fn, deps) 相当于 useMemo(() => fn, deps)
 * useMemo依赖性作用与useCallback相同
 */
export default function App() {
    const [list, setlist] = useState([
        "蜘蛛侠",
        "蝙蝠侠",
        "ABC",
        "999",
        "nft",
        "xiaoMI",
    ]);
    const [text, settext] = useState("");

    // const changeText = useCallback((e) => {
    //     settext(e.target.value);
    // }, []);

    // 使用useMemo替代useCallback
    const changeText = useMemo(
        () => (e) => {
            settext(e.target.value);
        },
        []
    );

    // 使用useMemo充当计算属性
    const showList = useMemo(
        () =>
            list.filter((item) =>
                item.toUpperCase().includes(text.toUpperCase())
            ),
        [text, list]
    );

    return (
        <div>
            <input type='text' onChange={(e) => changeText(e)} value={text} />
            <ul>
                {showList.map((item) => (
                    <li key={item}>{item}</li>
                ))}
            </ul>
        </div>
    );
}
