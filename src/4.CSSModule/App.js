import React from "react";
import "./CSS/app.css";
// 引入模块化css，命名为xxx.module.css的文件
// 这种文件会自动将文件内样式名称（class名或id名），转化为唯一的名称
// 该文件导出一个对象，对象内是一个个我们定义的名称:转化后的名称，所以在页面中使用时可以这样：
// className={style.我们定义的名称}
import style from "./CSS/app.module.css";
import Home from "./Home";
console.log(style);
export default function App() {
    return (
        <div>
            <h1>CSS模块化示例</h1>
            <p>
                在React中引入CSS，会发现CSS代码实际是添加到了html页面的head中，这样类名相同时会引发样式冲突问题，如:内容1
            </p>
            <p>
                使用CSSModule，自动将我们设置的类名转化为唯一的值，从而避免样式冲突
            </p>
            <hr />
            {/* .one是App组件定义的全局类名，会影响其它地方 */}
            <span className='one'>App组件内容1</span>
            {/* .borderBottom是App组件定义的全局类名，会影响其它地方*/}
            {/* #fontSize和.color是App组件内部模块化的样式名，不会影响其它地方  */}
            {/* 使用模块化类名时添加其它类名注意空格：" borderBottom" */}
            <span className={style.color + " borderBottom"} id={style.fontSize}>
                App组件内容2
            </span>
            <Home></Home>
        </div>
    );
}
