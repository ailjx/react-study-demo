import React from "react";

export default function Home() {
    return (
        <div>
            {/* .one是App组件定义的全局类名，会影响Home组件 */}
            <span className='one'>Home组件内容1</span>
            {/* .borderBottom是App组件定义的全局类名，会影响Home组件*/}
            {/* #fontSize和.color是App组件内部模块化的样式名，不会影响Home组件  */}
            <span className='color borderBottom' id='fontSize'>
                Home组件内容2
            </span>
        </div>
    );
}
