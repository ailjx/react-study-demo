import React from "react";
// children属性是什么
// 表示该组件的子节点，只要组件内部有子节点，props中就有该属性
// children可以是
// 1. 普通文本
// 2. 普通标签元素
// 3. 函数
// 4. JSX
const SonA = ({ children }) => {
    return <div>我是App的子组件SonA组件{children}</div>;
};

// 组件内部有多个根子节点时，children会被转换成数组
// 可直接使用chilren获取全部根子节点，也可使用下标获取指定的根子节点
const SonB = ({ children }) => {
    return (
        <div>
            我是App的子组件SonB组件 全部内容为：
            {children}
            内容1：
            {children[0]}
            内容2：
            {children[1]}
            内容3：
            {children[2]}
        </div>
    );
};

//父组件App
class App extends React.Component {
    state = {
        msg: "我是App组件的状态",
    };
    render() {
        return (
            <div className='App'>
                <SonA>
                    {/* 组件内部的子节点会当初props的children属性自动传给子组件 */}
                    <h1>我是props的属性内容children，{this.state.msg}</h1>
                </SonA>
                <SonB>
                    <h1>插入到SonB的1</h1>
                    <h1>插入到SonB的2</h1>
                    <h1>插入到SonB的3</h1>
                </SonB>
            </div>
        );
    }
}

export default App;

/**
 * 1. 为了复用
 * 2. 一定程度减少父子通信（因为children内容是存在与父组件的，能直接访问父组件的状态）
 */
