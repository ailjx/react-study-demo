import React, { Component } from "react";

/**
 * 利用发布订阅模式进行组件通信
 * 由B组件向A组件传递数据
 */
const bus = {
    list: [],
    //订阅
    subscribe(cb) {
        this.list.push(cb);
    },
    //发布
    publish(arg) {
        this.list.forEach((cb) => {
            cb && cb(arg);
        });
    },
};

// 订阅者
// bus.subscribe((arg) => {
//     console.log("1111", arg);
// });
// bus.subscribe((arg) => {
//     console.log("2222", arg);
// });

// 发布者
// bus.publish("我是发布者发布的参数");
// 注意顺序，订阅者要先订阅，发布者再发布才会有效果
class A extends Component {
    constructor() {
        super();
        this.state = {
            info: "",
        };
        //在组件初始化时开始订阅
        bus.subscribe((info) => {
            //订阅的事件处理
            //根据发布者发布的信息修改状态
            this.setState({
                info: info,
            });
        });
    }
    render() {
        return (
            <div>
                <h1> 列表参数：{this.state.info}</h1>
            </div>
        );
    }
}
class B extends Component {
    render() {
        return (
            <div>
                {/* 发布数据 */}
                <button onClick={() => bus.publish(this.props.item)}>
                    {this.props.item}
                </button>
            </div>
        );
    }
}
export default function App() {
    return (
        <div>
            <A></A>
            {[1, 2, 3, 4].map((item) => (
                <B key={item} item={item}></B>
            ))}
        </div>
    );
}
