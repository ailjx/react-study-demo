import React, { Component, PureComponent } from "react";
/**
 * React性能优化：
 * 1. shouldComponentUpdate生命周期，见13.1
 * 2. PureComponent
 *      PureComponent会帮你比较新旧props，state，决定shouldComponentUpdata返回true或false，从而决定要不要render
 *      注意：如果组件的state或props一直都在变，使用PureComponent并不会使性能变快，因为比较新老状态也需要花时间
 *           React.PureComponent 中的 shouldComponentUpdate() 仅作对象的浅层比较。
 *           如果对象中包含复杂的数据结构，则有可能因为无法检查深层的差别，产生错误的比对结果。
 *           仅在 props 和 state 较为简单时，才使用 React.PureComponent
 * */

//使用Component，只要修改了状态（无论新老状态是否相等），render函数都会被执行
class Com extends Component {
    state = {
        name: "小明",
    };
    render() {
        console.log("Com-->render");
        return (
            <div>
                {/* 每一次点击，组件都会被重新渲染 */}
                <button
                    onClick={() => {
                        this.setState({
                            name: "小红666",
                        });
                    }}>
                    修改Com状态
                </button>
                {this.state.name}
            </div>
        );
    }
}

//使用PureComponent，修改状态时如果新老状态相等，render函数不会被执行
class PureCom extends PureComponent {
    state = {
        name: "小明",
    };
    render() {
        console.log("PureCom-->render");
        return (
            <div>
                {/* 只有第一次点击，新老状态不同，组件重新渲染，之后点击不会重新渲染 */}
                <button
                    onClick={() => {
                        this.setState({
                            name: "小红666",
                            // name: [2.1], //name为数组或其它复杂类型时，PureComponent就失效了
                        });
                    }}>
                    修改PureCom状态
                </button>
                {this.state.name}
            </div>
        );
    }
}

export default class App extends Component {
    render() {
        return (
            <div>
                <Com></Com>
                <PureCom></PureCom>
            </div>
        );
    }
}
