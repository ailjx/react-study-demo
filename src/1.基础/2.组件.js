/*
 * @Author: AiLjx
 * @Date: 2022-05-12 18:21:50
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 18:55:59
 */
import React, { Component } from "react";

//函数式组件的创建和渲染
//创建：首字母要大写
//渲染：<Hello></hello>
function Hello() {
    return <span>hello</span>;
}

//类组件的创建和渲染
//创建：首字母要大写，类要继承React.Component父类，这样可以使用父类的render方法
//渲染：<HelloClass></HelloClass>
class HelloClass extends React.Component {
    render() {
        return <div>this is class Component</div>;
    }
}
//类组件也可以这样写，配合：import React, { Component } from "react";将Component解构出来
class Ahello extends Component {
    render() {
        return <div>999</div>;
    }
}
function App() {
    return (
        <div className='App'>
            {/* 渲染hello函数组件 */}
            <Hello></Hello>
            {/* 渲染HelloClass类组件 */}
            <HelloClass></HelloClass>
            <Ahello></Ahello>
        </div>
    );
}

export default App;
