import React from "react";
//B组件传数据给A组件
//过程：B先传给父组件App，App再传给A：即状态提升
const SonA = ({ msg }) => {
  return (
    <>
      <div>我是函数式A子组件：{msg}</div>
    </>
  );
};

class SonB extends React.Component {
  state = {
    message: <h1>我是B组件的数据</h1>,
  };
  render() {
    //使用this.props获取父组件传来的数据
    return (
      <>
        <div>我是类子组件</div>
        <button onClick={() => this.props.getBMsg(this.state.message)}>
          向A发数据
        </button>
      </>
    );
  }
}

//父组件App
class App extends React.Component {
  state = {
    message: "",
  };
  Appfn = () => {
    alert("我是父组件传来的函数");
  };
  getBMsg = (sonMsg) => {
    this.setState({
      message: sonMsg,
    });
  };
  render() {
    return (
      <div className='App'>
        <SonA msg={this.state.message}></SonA>
        <hr />
        <SonB getBMsg={this.getBMsg}></SonB>
      </div>
    );
  }
}

export default App;
