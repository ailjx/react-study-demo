import React, { Component } from "react";
/**
 * 利用getSnapshotBeforeUpdate实现容器顶部新加入元素时，我们正在查看的元素位置相对我们的视野保持不变
 * 核心原理：新加入元素时，使滚动条自动滚动，将我们正在查看的元素始终展现在我们看到的位置上
 */
export default class App extends Component {
    state = {
        list: [1, 2, 3, 4, 5, 6, 7, 8, 9],
    };
    ulRef = React.createRef();
    getSnapshotBeforeUpdate() {
        //此时拿到的scrollHeight是旧值，传给了componentDidUpdate第三个参数value
        console.log(this.ulRef.current.scrollHeight);
        return this.ulRef.current.scrollHeight;
    }
    componentDidUpdate(prevProps, prevState, value) {
        //此时拿到的scrollHeight是新值
        console.log(this.ulRef.current.scrollHeight);
        //将滚动条的上距离加上容器新的高度-老的高度（即新加入元素的高度）
        //实现我们查看的内容位置保持不变
        this.ulRef.current.scrollTop += this.ulRef.current.scrollHeight - value;
    }
    render() {
        return (
            <div>
                <button
                    onClick={() => {
                        this.setState({
                            list: [10, 11, 12, 13, 14, ...this.state.list],
                        });
                    }}>
                    添加
                </button>
                <h3>
                    滑动容器内容后点击添加按钮，可以看到滚动条自动变化了，容器内展示内容保持不变
                </h3>
                <ul
                    style={{
                        background: "red",
                        height: "300px",
                        overflow: "auto",
                    }}
                    ref={this.ulRef}>
                    {this.state.list.map((item, index) => (
                        <li
                            key={index}
                            style={{
                                height: "150px",
                                backgroundColor: "pink",
                                margin: "10px",
                            }}>
                            {item}
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}
