/**
 * 在createApp脚手架中使用反向代理：
 * 安装：npm i http-proxy-middleware --save
 * src目录下新建setupProxy.js（注意：新建这个文件后，若文件内内容不符合格式要求，我们的react应用会出现无法正常打开的请求）
 * 配置setupProxy.js，见src/setupProxy.js
 * 每次修改setupProxy.js都需要手动重启服务器
 */

import React from "react";

export default function App() {
    return <div>反向代理，查看src/setupProxy.js</div>;
}
