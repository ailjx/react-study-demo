const listName = [
    {
        id: 1,
        name: "加蓬",
    },
    {
        id: 2,
        name: "小明",
    },
    {
        id: 3,
        name: "大崇明",
    },
];
const AppName = "AppName";
const i = 6,
    active = true;
const NameFn = () => {
    return "加盟";
};
const getTag = (type) => {
    let tag;
    switch (type) {
        case 1:
            tag = <h1> this is one </h1>;
            break;
        case 2:
            tag = <h1> this is two </h1>;
            break;
        case 6:
            tag = <h1> this is 6 </h1>;
            break;
        default:
            break;
    }
    return tag;
};
const style = {
    color: "pink",
    fontSize: "40px",
};
const htmlText = `
                        <h1>我是富文本内容</h1>
                        <ul>
                            <li>1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                            <li>5</li>
                        </ul>
                        `;

function App() {
    return (
        //幽灵标签<></>，只有有一个根节点，可以使用幽灵标签
        <>
            <div className='App'>
                <h1 style={{ color: "red", fontSize: "30px" }}>
                    {" "}
                    {i > 7 ? AppName : NameFn()}{" "}
                </h1>{" "}
                {i === 6 ? (
                    <div>
                        <span> this is a </span>{" "}
                    </div>
                ) : null}{" "}
                <button style={style}> +1 </button> {getTag(2)}{" "}
                <ul className={active ? "ulLi" : "jj"}>
                    {" "}
                    {listName.map((item) => (
                        <li key={item.id}> {item.name} </li>
                    ))}{" "}
                </ul>{" "}
            </div>{" "}
            <div> 9999 </div>{" "}
            <div>
                {" "}
                {/* 普通展示无法显示富文本内容 */} <span> {htmlText} </span>{" "}
                {/ * 使用dangerouslySetInnerHTML显示富文本内容 * /}{" "}
                <span
                    dangerouslySetInnerHTML={{
                        __html: htmlText,
                    }}>
                    {" "}
                </span>{" "}
            </div>{" "}
        </>
    );
}

export default App;
