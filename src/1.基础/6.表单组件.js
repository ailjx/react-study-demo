/*
 * @Author: AiLjx
 * @Date: 2022-05-12 20:25:47
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 18:59:47
 */
import React, { createRef, useRef } from "react";
//受控表单组件
class HelloClass extends React.Component {
    //1.声明用来控制input value的react组件状态
    state = {
        message: "this is message",
    };
    //事件回调函数
    inputChange = (e) => {
        //4.拿到输入框最新的值，交给state中的message
        this.setState({
            message: e.target.value,
        });
    };
    render() {
        // 2.给input框的value属性绑定react state
        //3.给input框绑定一个change的事件
        return (
            <input
                type='text'
                onChange={this.inputChange}
                value={this.state.message}
            />
        );
    }
}
//非受控表单组件
class HelloClass2 extends React.Component {
    //msgRef可以自定义
    msgRef = createRef(null);
    state = {
        message: "this is message",
    };
    //事件回调函数
    getValue = () => {
        //通过msgRef获取input Value的值
        console.log(this.msgRef.current.value);
    };
    render() {
        // 2.给input框的value属性绑定react state
        //3.给input框绑定一个change的事件
        return (
            <>
                <input type='text' ref={this.msgRef} />
                <button onClick={this.getValue}>点击获取输入框的值</button>
            </>
        );
    }
}
//函数式组件使用ref
const HelloClass3 = () => {
    // 在函数式组件中推荐使用useRef而不是createRef
    // useRef是hooks的一种
    // 由useRef创建的ref对象在组件的整个生命周期内都不会改变
    // 但是由createRef创建的ref对象，组件每更新一次，ref对象就会被重新创建。
    const msgRef = useRef(null);
    //事件回调函数
    const getValue = () => {
        //通过msgRef获取input Value的值
        console.log(msgRef.current.value);
    };
    return (
        <>
            <input type='text' ref={msgRef} />
            <button onClick={getValue}>点击获取输入框的值</button>
        </>
    );
};
function App() {
    return (
        <div className='App'>
            <HelloClass></HelloClass>
            <hr />
            <HelloClass2></HelloClass2>
            <hr />
            <HelloClass3></HelloClass3>
        </div>
    );
}

export default App;
