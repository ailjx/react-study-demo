import React from "react";

//函数式组件props默认值
//1.使用defaultProps（半淘汰）
const SonA = (props) => {
    return <div>我是组件SonA:{props.name}</div>;
};
//设置props默认值
SonA.defaultProps = {
    name: "CBJ",
};
//2.函数参数默认值（推荐的方案）
const SonAT = ({ name = "cbj" }) => {
    return <div>我是组件SonA:{name}</div>;
};

//类组件props默认值
//1.与函数式组件一样使用defaultProps，略
//2.static类静态属性定义(推荐)
class SonB extends React.Component {
    static defaultProps = {
        name: "cbj66",
    };
    render() {
        return <div>我是SonB组件：{this.props.name}</div>;
    }
}

//父组件App
class App extends React.Component {
    render() {
        return (
            <div className='App'>
                <SonA></SonA>
                <SonAT></SonAT>
                <SonB></SonB>
            </div>
        );
    }
}

export default App;
