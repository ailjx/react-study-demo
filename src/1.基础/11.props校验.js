import React from "react";
import PropTypes from "prop-types";
//官方文档：
//https://react.docschina.org/docs/typechecking-with-proptypes.html
const SonA = () => {
  return <div>我是App的子组件SonA组件</div>;
};
//对props校验
SonA.propTypes = {
  list: PropTypes.array.isRequired, //限定传给SonA的list属性必须为数组且是必传项
};
//父组件App
class App extends React.Component {
  render() {
    return (
      <div className='App'>
        <SonA list={6}></SonA>
      </div>
    );
  }
}

export default App;
