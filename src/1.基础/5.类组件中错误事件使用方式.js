/*
 * @Author: AiLjx
 * @Date: 2022-05-12 19:39:13
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 18:59:43
 */
import React from "react";
//类组件中使用事件的错误方式
class HelloClass extends React.Component {
    state = {
        name: "cbj",
    };
    //事件回调函数
    changeName() {
        //如果直接this.changeName调用会报错
        console.log(this); //undefined
        // 写普通函数，是拿不到当前实例的this的
        //因为this为undefined，修改状态无效
        this.setState({
            name: "陈",
        });
    }
    render() {
        //render函数中的this已经被react内部做了修正，指向当前的组件实例对象
        return (
            <div>
                <button onClick={this.changeName}>错误方式</button>
                {/* 如果不用constructor做修正，直接可以在事件绑定的位置通过箭头函数的写法，直接沿用父函数中的this指向 */}
                <button onClick={() => this.changeName()}>
                    {this.state.name}纠正方式1
                </button>
            </div>
        );
    }
}
//若事件非要写普通函数的形式，并且调用函数时不用箭头函数的写法，则可以用constructor纠正
class HelloClassSuccess extends React.Component {
    constructor() {
        super();
        //在类组件初始化阶段，使用bind强行将回调函数的this修正到永远指向当前组件实例对象
        this.changeName = this.changeName.bind(this);
    }
    state = {
        name: "cbj",
    };
    //事件回调函数
    changeName() {
        console.log(this);
        this.setState({
            name: "陈",
        });
    }
    render() {
        // 2.使用状态
        return (
            <div>
                <button onClick={this.changeName}>
                    {this.state.name}纠正方式2
                </button>
            </div>
        );
    }
}
//可见上述操作的麻烦，所以标准的用法是使用箭头函数声明事件：见event.jsx
//因为箭头函数的this永远指向父作用域，不会改变
//而普通函数的this指向是可变的
function App() {
    return (
        <div className='App'>
            {/* 渲染HelloClass类组件 */}
            <HelloClass></HelloClass>
            <HelloClassSuccess></HelloClassSuccess>
        </div>
    );
}

export default App;
