/*
 * @Author: AiLjx
 * @Date: 2022-06-28 14:29:40
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:39:20
 */
import { takeEvery, put, call } from "redux-saga/effects";
import getListAction from "../actionCreator/getListAction";
import getNameAction from "../actionCreator/getNameAction";

function* watchSaga() {
    yield takeEvery("get-list", getList);
    yield takeEvery("get-name", getName);
}

function* getList() {
    let res = yield call(getListAction);
    console.log(res);
    yield put({
        type: "change-list",
        payLoad: res,
    });
}

function* getName() {
    let res = yield call(getNameAction);
    console.log(res);
    yield put({
        type: "change-name",
        payLoad: res,
    });
}

export default watchSaga;
