/*
 * @Author: AiLjx
 * @Date: 2022-06-28 14:29:40
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:28:54
 */
import { take, fork, put, call } from "redux-saga/effects";
import { getListAction1, getListAction2 } from "../actionCreator/getListAction";

function* watchSaga() {
    while (true) {
        yield take("get-list");
        yield fork(getList);
    }
}

function* getList() {
    let res1 = yield call(getListAction1); //阻塞的调用fn
    // 将getListAction1返回值传给getListAction2，实现链式调用
    let res2 = yield call(getListAction2, res1);
    console.log(res2); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
    yield put({
        type: "change-list",
        payLoad: res2,
    });
}

export default watchSaga;
