/*
 * @Author: AiLjx
 * @Date: 2022-06-28 12:56:59
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:26:35
 */

import React from "react";
import store from "./redux/store";
export default function App() {
    const getList = () => {
        if (store.getState().list.length === 0) {
            store.dispatch({
                type: "get-list",
            });
        } else {
            console.log("缓存", store.getState().list);
        }
    };
    return (
        <div>
            <button onClick={getList}>发起请求</button>
        </div>
    );
}
