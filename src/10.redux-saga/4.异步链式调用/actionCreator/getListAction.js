/*
 * @Author: AiLjx
 * @Date: 2022-06-28 14:21:08
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:28:36
 */
function getListAction1() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve([1, 2, 3]);
        }, 1000);
    });
}
function getListAction2(data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve([...data, 4, 5, 6, 7, 8, 9]);
        }, 1000);
    });
}

export { getListAction1, getListAction2 };
