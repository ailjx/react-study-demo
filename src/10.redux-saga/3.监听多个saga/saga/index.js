/*
 * @Author: AiLjx
 * @Date: 2022-06-28 15:16:41
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:20:52
 */
import { all } from "redux-saga/effects";
import watchListSaga from "./ListSaga";
import watchNameSaga from "./NameSaga";
function* watchSage() {
    // 使用all函数将多个saga聚合
    yield all([watchListSaga(), watchNameSaga()]);
}

export default watchSage;
