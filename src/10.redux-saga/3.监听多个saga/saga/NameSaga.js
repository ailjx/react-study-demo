/*
 * @Author: AiLjx
 * @Date: 2022-06-28 15:16:08
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:19:37
 */

import { take, fork, put, call } from "redux-saga/effects";
import getNameAction from "../actionCreator/getNameAction";

function* watchNameSaga() {
    while (true) {
        yield take("get-name");
        yield fork(getName);
    }
}

function* getName() {
    let res = yield call(getNameAction);
    console.log(res);
    yield put({
        type: "change-name",
        payLoad: res,
    });
}

export default watchNameSaga;
