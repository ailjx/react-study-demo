/*
 * @Author: AiLjx
 * @Date: 2022-06-28 14:24:25
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:14:12
 */
const reducer = (
    prevState = {
        list: [],
        name: "",
    },
    action
) => {
    let newState = { ...prevState };
    switch (action.type) {
        case "change-list":
            newState.list = action.payLoad;
            return newState;
        case "change-name":
            newState.name = action.payLoad;
            return newState;
        default:
            return prevState;
    }
};

export default reducer;
