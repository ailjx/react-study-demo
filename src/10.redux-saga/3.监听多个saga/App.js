/*
 * @Author: AiLjx
 * @Date: 2022-06-28 12:56:59
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:14:36
 */

import React from "react";
import store from "./redux/store";
export default function App() {
    const getList = () => {
        if (store.getState().list.length === 0) {
            store.dispatch({
                type: "get-list",
            });
        } else {
            console.log("缓存", store.getState().list);
        }
    };
    const getName = () => {
        store.dispatch({
            type: "get-name",
        });
    };
    return (
        <div>
            <button onClick={getList}>获取列表</button>
            <button onClick={getName}>获取姓名</button>
        </div>
    );
}
