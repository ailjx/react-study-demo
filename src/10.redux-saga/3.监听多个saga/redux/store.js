/*
 * @Author: AiLjx
 * @Date: 2022-06-28 14:22:45
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:19:06
 */
import { createStore, applyMiddleware } from "redux";
import reducer from "../reducers/reducer";
import createSagaMiddleware from "redux-saga";
import watchSaga from "../saga/index";

// 先创建saga，在应用中间件
const SagaMidlleware = createSagaMiddleware();
const store = createStore(reducer, applyMiddleware(SagaMidlleware));

// 运行saga监听者watchSaga
SagaMidlleware.run(watchSaga); // saga任务

export default store;
