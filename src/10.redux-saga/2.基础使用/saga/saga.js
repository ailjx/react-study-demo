/*
 * @Author: AiLjx
 * @Date: 2022-06-28 14:29:40
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 14:58:43
 */
import { take, fork, put, call } from "redux-saga/effects";
import getListAction from "../actionCreator/getListAction";

function* watchSaga() {
    while (true) {
        // take 根据type监听组件发来的action
        yield take("get-list");
        // fork 非阻塞调用的形式执行fn
        yield fork(getList);
    }
}

function* getList() {
    // 异步处理
    // call函数发异步请求
    let res = yield call(getListAction); //阻塞的调用fn
    console.log(res);
    // put函数发出新的action，交给reducer处理，非阻塞执行
    yield put({
        type: "change-list",
        payLoad: res,
    });
}

export default watchSaga;
