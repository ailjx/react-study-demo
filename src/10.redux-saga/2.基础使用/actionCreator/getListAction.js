/*
 * @Author: AiLjx
 * @Date: 2022-06-28 14:21:08
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:12:22
 */
export default function getListAction() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve([1, 2, 3]);
        }, 1000);
    });
}
