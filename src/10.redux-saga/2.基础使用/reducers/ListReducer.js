/*
 * @Author: AiLjx
 * @Date: 2022-06-28 14:24:25
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 14:27:01
 */
const ListReducer = (
    prevState = {
        list: [],
    },
    action
) => {
    let newState = { ...prevState };
    switch (action.type) {
        case "change-list":
            newState.list = action.payLoad;
            return newState;

        default:
            return prevState;
    }
};

export default ListReducer;
