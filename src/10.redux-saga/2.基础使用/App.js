/*
 * @Author: AiLjx
 * @Date: 2022-06-28 12:56:59
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 15:09:49
 */
// 之前处理redux异步问题使用的是中间件，那是侵入性的方式，使用redux-saga是非侵入性的方式
// redux-saga处理redux在异步管理中的流程，本质使用的是ES6中的Generator函数
// 在saga中，全局监听器和接收器使用Generator函数（生成器函数）和saga自身的一些辅助函数实现对整个流程的管控
// 安装：npm i redux-saga
import React from "react";
import store from "./redux/store";
export default function App() {
    const getList = () => {
        if (store.getState().list.length === 0) {
            store.dispatch({
                type: "get-list",
                // 这里的type并不是reducer里面进行判断的action.type，
                // 而是saga进行监听的type
            });
        } else {
            console.log("缓存", store.getState().list);
        }
    };
    return (
        <div>
            <button onClick={getList}>发起请求</button>
        </div>
    );
}
