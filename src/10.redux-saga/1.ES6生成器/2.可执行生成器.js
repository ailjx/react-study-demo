/*
 * @Author: AiLjx
 * @Date: 2022-06-28 13:37:25
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 14:17:51
 */

// 设三个链式调用的异步函数
function getData1() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`data1开始执行`);
            resolve(`data1`);
        }, 1000);
    });
}
function getData2(data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`data2接收的参数为--------：${data}`);
            resolve(`data2`);
        }, 1000);
    });
}
function getData3(data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`data3接收的参数为--------：${data}`);
            resolve(`data3`);
        }, 1000);
    });
}

function* gen() {
    let f1 = yield getData1();
    console.log("f1-->", f1); // data1
    let f2 = yield getData2(f1);
    console.log("f2-->", f2); // data2
    let f3 = yield getData3(f2);
    console.log("f3-->", f3); // data3
} // 单看这个函数是不是就很像async await的写法了

// 定义一个自动执行生成器的函数
function run(fu) {
    let g = fu();
    function next(data) {
        let result = g.next(data);

        // next返回值中done为true时代表生成器执行完毕
        if (result.done) {
            return result.value;
        }
        // next返回值中value即为我们在yield后写的Promise函数
        result.value.then((res) => {
            // 将Promise最后返回的值作为参数传给下一个
            next(res);
        });
    }
    next();
}
run(gen);

export default function App() {
    return <div>2.可执行生成器</div>;
}
