/*
 * @Author: AiLjx
 * @Date: 2022-06-28 13:01:32
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-28 14:17:17
 */

import React from "react";
// *和yield是生成器函数的两个重要标识
// 完全正确的写法并实现async await的基本原理见：2.可执行生成器
function* test() {
    console.log(111);
    yield "111-输出"; // yield相当于中断点，后面的值既是next函数的返回值
    console.log(222);
    let input3 = yield; // yield能返回下一个next传来的参数
    console.log(333, input3);
    yield;
}
// 记录生成器返回值
let t = test();
// next函数让生成器函数走一次，推一把走一次，遇到yield停下
let res1 = t.next(); // log：111
console.log(res1); // log：{value: '111-输出', done: false} （done表示生成器函数是否全部走完）

let res2 = t.next(); // log：222
console.log(res2);

let res3 = t.next("ccc"); // log：333 'ccc' （next函数的参数会传给next开始执行的那个yield）
console.log(res3);

let res4 = t.next(); // log：
console.log(res4); // log：{value: undefined, done: true} （done:true，生成器函数执行结束）

// ES7 async await成了生成器函数的替代方案

// 生成器在异步中的应用：
// 错误用法：
// function* test1() {
//     setTimeout(() => {
//         console.log("111-success");
//     }, 1000);
//     yield;
//     setTimeout(() => {
//         console.log("222-success");
//     }, 1000);
//     yield;
//     setTimeout(() => {
//         console.log("333-success");
//     }, 1000);
//     yield;
// }

// const t1 = test1();
// t1.next();
// t1.next();
// t1.next(); // next函数是立即执行的，这三个执行后会发现111-success，222-success，333-success是同时打印出来的

// 也不太正确的用法：
function* test1() {
    setTimeout(() => {
        console.log("111-success");
        t1.next(); // 这个执行完毕在继续next
    }, 1000);
    yield;
    setTimeout(() => {
        console.log("222-success");
        t1.next();
    }, 1000);
    yield;
    setTimeout(() => {
        console.log("333-success");
    }, 1000);
    yield;
}

const t1 = test1();
t1.next();

export default function App() {
    return <div>1.生成器</div>;
}
