/*
 * @Author: AiLjx
 * @Date: 2022-05-12 11:58:29
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 22:46:33
 */
//src根目录下的index.js
import React from "react";
import ReactDOM from "react-dom/client";
//需要展示案例的App组件路径
import App from "./3.路由/V6版本/6.useRoutes配置路由/App";

// import { Provider } from "mobx-react";
// import store from "./5.mobx状态管理/V5版本/2.mobx-react/mobx/store";

// import { Provider } from "react-redux";
// import store from "./6.redux状态管理/6.react-redux/redux/store";

// import { store, persistor } from "./6.redux状态管理/8.redux持久化/redux/store";

// import { PersistGate } from "redux-persist/integration/react";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    // React严格模式
    // <React.StrictMode>
    //     <App />
    // </React.StrictMode>

    // 在这里使用Provider包裹传数据，代表数据只用一次（静态数据），不会修改
    //  <context.Provider value={10}>
    //         <App />
    //     </context.Provider >

    // 类组件使用mobx-react时，需要使用mobx-react导出的Provider包裹App组件，并传入对应的store
    // <Provider store={store}>
    //     <App></App>
    // </Provider>

    // 使用React-Redux导出的Provider包裹App组件，并传入对应的store
    // React-Redux 提供Provider组件，可以让容器组件拿到state。
    // <Provider store={store}>
    //     <App />
    // </Provider>

    // 使用React-Redux导出的Provider包裹App组件，并传入对应的store
    // <Provider store={store}>
    //     {/* 使用PersistGate包裹App，实现redux持久化操作 */}
    //     <PersistGate loading={null} persistor={persistor}>
    //         <App />
    //     </PersistGate>
    // </Provider>

    <App></App>
);
