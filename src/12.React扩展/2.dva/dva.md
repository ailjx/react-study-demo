<!--
 * @Author: AiLjx
 * @Date: 2022-06-30 09:15:33
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 15:54:42
-->

`dva` 首先是一个基于 `redux`和 `redux-saga` 的数据流方案，然后为了简化开发体验，`dva` 还额外内置了 `react-router` 和 `fetch`，所以也可以理解为一个**轻量级的应用框架**。

在目前已经**不推荐单独创建 dva 项目**，因为 dva 已经**停止更新**，并可以在` umi``中集成 `dva`，
但这不影响我们继续创建 dva 项目进行学习，后续在 umi 部分也会学习到集成 dva。

官网：https://dvajs.com

安装脚手架：`npm install dva-cli -g`

创建新应用：`dva new 项目名称`

> 创建新应用时会提示：`dva-cli`已经弃用，建议使用`umi`，你是否坚持使用`dva-cli`？

> `dva-cli is deprecated, please use create-umi instead, checkout https://umijs.org/guide/create-umi-app.html for detail. 如果你是蚂蚁金服内部用户，请使用 bigfish 创建项目，详见 https://bigfish.alipay.com/ 。`

> 我们只是安装用来学习，不做生产需要，输入`y`继续安装即可

我们已将`dva` 项目（`dva-react`）安装到了总项目根目录下（与 `src` 同级）

打开 `dva-react` 终端`npm start`运行

> 如果缺少`node_modules`文件，先输入`npm i` 安装依赖

> 启动项目后控制台会有报错出现，这大部分是由于`dva-cli`很久未更新，在不影响使用的情况下可忽略该错误

之后在`dva-react`内进行学习，`dva-react`是一个综合的小项目，内含`路由`、`redux`、`redux-saga`、`配置跨域`、`本地测试mock接口`等内容
