<!--
 * @Author: AiLjx
 * @Date: 2022-06-30 16:44:14
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 18:26:29
-->

`Umi` 官网：https://v3.umijs.org/zh-CN/docs/getting-started

`umi`，中文可发音为乌米，是一个可插拔的企业级 `react` 应用框架。`umi` 以路由为基础的，支持类 `next.js` 的`约定式路由`，以及各种进阶的路由功能，并以此进行功能扩展，比如支持路由级的按需加载。`umi `在约定式路由的功能层面会更像 `nuxt.js` 一些。开箱即用，省去了搭框架的时间

前面说过了 `dva`，知道 `dva` 其实已经不建议单独使用了，在开发大型项目时推荐使用 `umi`，它内部集成了 `react` 全家桶，并可集成 `dva`

安装：

1. 新建一个文件夹
2. 打开新建文件夹的终端：`npx @umijs/create-umi-app`
3. `npm i` 安装依赖

我们已将`umi` 项目（`umi-react`）安装到了总项目根目录下（与 `src` 同级）

打开 `umi-react` 终端`npm start`运行

> 如果缺少`node_modules`文件，先输入`npm i` 安装依赖

之后在`umi-react`内进行学习，`umi-react`是一个综合的小项目，内含`约定式路由的使用`、`集成dva使用redux和dva-loading`、`配置跨域`、`本地测试mock接口`、`antd-model的使用`等内容，对于一个小 `demo`，方便起见，其中并没有着重规范使用`TS语法`（大多都是偷懒直接` any`）
