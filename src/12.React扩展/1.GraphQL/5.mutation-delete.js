/*
 * @Author: AiLjx
 * @Date: 2022-06-29 18:29:11
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 22:28:19
 */

import { ApolloProvider, Mutation } from "react-apollo";

import ApolloClient from "apollo-boost";

import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "/graphql",
});

export default function App() {
    return (
        <ApolloProvider client={client}>
            <div>
                <Delete></Delete>
            </div>
        </ApolloProvider>
    );
}

function Delete() {
    const myquery = gql`
        mutation ($id: String!) {
            deleteFilm(id: $id)
        }
    `;

    return (
        <Mutation mutation={myquery}>
            {(cb, { data }) => {
                console.log(data);
                return (
                    <div>
                        <button
                            onClick={() => {
                                cb({
                                    variables: {
                                        // 这个id是写死的，用于测试
                                        // 若ID失效，去数据库查看任一条数据的ID进行替换即可
                                        id: "62bc4c5b58f5f8060c864bed",
                                    },
                                });
                            }}>
                            删除
                        </button>
                    </div>
                );
            }}
        </Mutation>
    );
}
