/*
 * @Author: AiLjx
 * @Date: 2022-06-29 18:29:11
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 23:36:46
 */
// graphql可以让前端人员发请求时需要哪个字段就获取哪个字段，
// 当然这需要后端的配合（见总项目根目录下server后端文件）

// 安装：npm i react-apollo apollo-boost graphql graphql-tag
// apollo模块是对graphql的一种封装，使我们能够组件式的使用graphql
// apollo-boost启动模块，graphql核心模块，graphql-tag是graphql描述标签

// ApolloProvider提供全局客户端配置，Query是请求组件，用来发起请求获取数据
import { ApolloProvider, Query } from "react-apollo";

// ApolloClient设置客户端配置，提供请求地址
import ApolloClient from "apollo-boost";

// 使用graphql-tag生成graphql请求描述语句，这里gql是自定义的
import gql from "graphql-tag";

// ApolloClient设置客户端配置，提供请求地址
const client = new ApolloClient({
    // 请求地址，实际是http://localhost:3001/graphql
    // src/setupProxy.js中已经做好跨域
    uri: "/graphql",
});

export default function App() {
    return (
        // ApolloProvider原理也是使用Provider向子组件分发数据
        <ApolloProvider client={client}>
            <div>
                <List></List>
            </div>
        </ApolloProvider>
    );
}

function List() {
    // 使用graphql-tag生成graphql请求描述语句
    // 需要获取哪个字段就填哪个字段，这里是获取三个字段id name poster
    const query = gql`
        query {
            getNowplayingList {
                id
                name
                poster
            }
        }
    `;
    return (
        // Query是react-apollo封装的graphql查询请求组件，使查询请求组件化
        // 将graphql的query请求描述传给Query组件query props
        <Query query={query}>
            {/* Query组件内部有一个回调函数，参数是请求的结果 */}
            {/* 可以解构参数，获得loading标志位（是否正在请求，为false时代表请求完毕）
            data数据，refetch发送（调用refetch能使请求重新发送，具体使用见：小案例/3.GraphQl使用） */}
            {({ loading, data, refetch }) => {
                console.log(loading, data);
                return (
                    <div>
                        {/* 因为请求是异步问题，在创建组件时Query会被创建两次，
                        第一次是组件初始化，此时还没有请求到数据，loading为true，此时直接渲染数据会报错
                        第二次是请求到了数据，使组件重新创建更新，loading为false */}
                        {!loading && (
                            <ul>
                                {data.getNowplayingList.map((item) => (
                                    <li key={item.id}>
                                        {item.name}--{item.poster}
                                    </li>
                                ))}
                            </ul>
                        )}
                    </div>
                );
            }}
        </Query>
    );
}
