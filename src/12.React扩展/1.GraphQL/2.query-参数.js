/*
 * @Author: AiLjx
 * @Date: 2022-06-29 18:29:11
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 22:23:04
 */

import { ApolloProvider, Query } from "react-apollo";

import ApolloClient from "apollo-boost";

import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "/graphql",
});

export default function App() {
    return (
        <ApolloProvider client={client}>
            <div>
                <List></List>
            </div>
        </ApolloProvider>
    );
}

function List() {
    // query后可以添加接收参数的名为id（前面需要加$，固定写法），类型对应后端的String!类型
    // getNowplayingList后可以接收id，该id的值为query收到的$id
    const myquery = gql`
        query ($id: String!) {
            getNowplayingList(id: $id) {
                id
                name
            }
        }
    `;
    // 也可以这样写
    //     const query = gql`
    //     query getNowplayingList($id: String!) {
    //         getNowplayingList(id: $id) {
    //             id
    //             name
    //         }
    //     }
    // `;

    // 这个id是写死的，用于测试
    // 若ID失效，去数据库查看任一条数据的ID进行替换即可
    const myid = "62bc16e8918ac61b4cddd9ee";
    return (
        // variables固定字段，接收一个对象，对象内是需要传的内容
        // id这个key对应的是myquery中$id
        <Query query={myquery} variables={{ id: myid }}>
            {({ loading, data }) => {
                console.log(loading, data);
                return (
                    <div>
                        {!loading && (
                            <ul>
                                {data.getNowplayingList.map((item) => (
                                    <li key={item.id}>
                                        {/* 会发现获取不到poster，因为graphql请求描述中
                                        并没有指定需要获取poster字段 */}
                                        {item.name}--{item.poster}
                                    </li>
                                ))}
                            </ul>
                        )}
                    </div>
                );
            }}
        </Query>
    );
}
