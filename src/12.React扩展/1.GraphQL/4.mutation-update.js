/*
 * @Author: AiLjx
 * @Date: 2022-06-29 18:29:11
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 22:23:30
 */

import { ApolloProvider, Mutation } from "react-apollo";

import ApolloClient from "apollo-boost";

import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "/graphql",
});

export default function App() {
    return (
        <ApolloProvider client={client}>
            <div>
                <Update></Update>
            </div>
        </ApolloProvider>
    );
}

function Update() {
    const myquery = gql`
        mutation ($id: String!, $input: FilmInput) {
            updateFilm(id: $id, input: $input) {
                id
                name
            }
        }
    `;

    return (
        // Mutation是react-apollo封装的graphql整删改请求组件，使请求组件化
        // 将graphql mutation请求描述传给Mutation组件mutation props
        <Mutation mutation={myquery}>
            {/* Mutation组件内部有一个回调函数，第一个参数是发送请求的方法
            它接收一个对象，对象内添加variables对象，在variables对象内添加请求参数
            Mutation组件第二个参数是请求的结果，可解构出请求返回数据的data */}
            {(cb, { data }) => {
                console.log(data);
                return (
                    <div>
                        {/* 点击按钮发送请求 */}
                        <button
                            onClick={() => {
                                cb({
                                    // variables固定字段，接收一个对象，对象内是需要传的内容
                                    variables: {
                                        // 这个id是写死的，用于测试
                                        // 若ID失效，去数据库查看任一条数据的ID进行替换即可
                                        id: "62bc1b384bf13d35207ea26f",
                                        input: {
                                            name: "修改啦啦啦啦",
                                            poster: "http://asdas",
                                            price: 1111111,
                                        },
                                    },
                                });
                            }}>
                            修改
                        </button>
                    </div>
                );
            }}
        </Mutation>
    );
}
