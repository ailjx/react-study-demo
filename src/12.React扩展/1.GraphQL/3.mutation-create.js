/*
 * @Author: AiLjx
 * @Date: 2022-06-29 18:29:11
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 21:10:07
 */

import { ApolloProvider, Mutation } from "react-apollo";

import ApolloClient from "apollo-boost";

import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "/graphql",
});

export default function App() {
    return (
        <ApolloProvider client={client}>
            <div>
                <Add></Add>
            </div>
        </ApolloProvider>
    );
}

function Add() {
    const myquery = gql`
        mutation ($input: FilmInput) {
            createFilm(input: $input) {
                id
                name
                price
            }
        }
    `;

    return (
        // Mutation是react-apollo封装的graphql整删改请求组件，使请求组件化
        // 将graphql mutation请求描述传给Mutation组件mutation props
        <Mutation mutation={myquery}>
            {/* Mutation组件内部有一个回调函数，第一个参数是发送请求的方法
            它接收一个对象，对象内添加variables对象，在variables对象内添加请求参数
            Mutation组件第二个参数是请求的结果，可解构出请求返回数据的data */}
            {(cb, { data }) => {
                console.log(data);
                return (
                    <div>
                        {/* 点击按钮发送请求 */}
                        <button
                            onClick={() => {
                                cb({
                                    // variables固定字段，接收一个对象，对象内是需要传的内容
                                    variables: {
                                        input: {
                                            name: "测试",
                                            poster: "9999",
                                            price: 18,
                                        },
                                    },
                                });
                            }}>
                            增加
                        </button>
                    </div>
                );
            }}
        </Mutation>
    );
}
