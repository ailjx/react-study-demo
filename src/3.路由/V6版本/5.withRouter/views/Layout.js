/*
 * @Author: AiLjx
 * @Date: 2022-07-01 21:34:37
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 22:18:12
 */
import { Outlet } from "react-router-dom";
import ProNav from "../components/ProNav";

function Layout(props) {
    return (
        <div>
            <ProNav></ProNav>
            Layout页面，以下是我的嵌套路由页面
            <hr />
            {/* 嵌套此页面的路由配置 */}
            <Outlet></Outlet>
        </div>
    );
}
export default Layout;
