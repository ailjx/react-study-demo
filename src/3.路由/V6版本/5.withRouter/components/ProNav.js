/*
 * @Author: AiLjx
 * @Date: 2022-06-11 19:54:31
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 22:18:26
 */
import { Component } from "react";

// 在V5版本/3.声明式和编程式导航中介绍了withRouter，使用withRouter能让组件无论在什么地方
// 都能获取到Route传递的路由props，也说到这在无法使用hooks的类组件中经常用到，在V6版本
// 中已经移除了withRouter（因为类组件正在逐渐成为过去式），但若你执意使用类组件，或正在
// 维护老项目，需要在V6版本下使用withRouter，那咱们就可以自己封装一个withRouter组件
import withRouter from "./withRouter";

class ProNav extends Component {
    // 用法和V5版本完全一致
    // this.props.history.push 跳转页面
    // this.props.history.match 获取参数
    // this.props.history.location 获取当前路由
    nav = [
        {
            name: "首页",
            path: "/",
        },
        {
            name: "关于",
            path: "/about",
        },
    ];
    handleChangePage = (path) => {
        this.props.history.push(path);
    };

    render() {
        console.log(this.props);
        return (
            <div>
                <ul>
                    <li>这是编程式导航</li>

                    {this.nav.map((item) => (
                        <button
                            key={item.path}
                            onClick={() => this.handleChangePage(item.path)}>
                            {item.name}
                        </button>
                    ))}
                </ul>
            </div>
        );
    }
}

export default withRouter(ProNav);
