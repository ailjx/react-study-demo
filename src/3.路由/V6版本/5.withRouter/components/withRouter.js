/*
 * @Author: AiLjx
 * @Date: 2022-07-01 21:51:14
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 22:16:13
 */
import React from "react";
import { useNavigate, useParams, useLocation } from "react-router-dom";
// 在V5版本中，类组件是这样使用路由：
// this.props.history.push 跳转页面
// this.props.history.match 获取参数
// this.props.history.location 获取当前路由
export default function withRouter(Component) {
    return function (props) {
        // 按照V5版本的的使用方式来取名
        const push = useNavigate();
        const match = useParams();
        const location = useLocation();

        // 仿照V5版本的withRouter来封装

        // 使用{...props}原因：外部使用withRouter包裹的组件时传递的props实际是
        // 传给了withRouter return的函数，该函数需要将其向下传递给组件。
        return <Component {...props} history={{ push, match, location }} />;
    };
}
