/*
 * @Author: AiLjx
 * @Date: 2022-07-01 21:34:37
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 21:43:43
 */
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Layout from "../views/Layout";
import Home from "../views/layout/Home";
import About from "../views/layout/About";

export default function IndexRouter({ children }) {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Layout></Layout>}>
                    <Route index element={<Home></Home>} />
                    <Route path='about' element={<About></About>} />
                </Route>
            </Routes>
        </BrowserRouter>
    );
}
