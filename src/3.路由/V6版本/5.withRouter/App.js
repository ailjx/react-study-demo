/*
 * @Author: AiLjx
 * @Date: 2022-07-01 21:34:37
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 21:40:07
 */
import IndexRouter from "./router/IndexRouter";
export default function App() {
    return (
        <div>
            <IndexRouter></IndexRouter>
        </div>
    );
}
