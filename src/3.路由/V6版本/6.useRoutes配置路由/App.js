/*
 * @Author: AiLjx
 * @Date: 2022-05-15 21:54:57
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 22:46:18
 */
import { BrowserRouter, Link } from "react-router-dom";
import MyRouter from "./router";
//路由配置
function App() {
    return (
        <BrowserRouter>
            <Link to='/'>首页 </Link>
            <Link to='/login'>登录 </Link>
            <MyRouter></MyRouter>
        </BrowserRouter>
    );
}

export default App;
