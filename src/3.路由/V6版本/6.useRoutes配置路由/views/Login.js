import { useNavigate } from "react-router-dom";

function Login() {
    const navigate = useNavigate();
    function goAbout() {
        navigate("/about?id=SearchParams", { replace: true });
    }
    function goAbout2() {
        navigate("/about/Params", { replace: true });
    }
    //跳转到关于项
    return (
        <div>
            Login页面
            <button onClick={goAbout}>SearchParams传参跳转到about</button>
            <button onClick={goAbout2}>Params传参跳转到about</button>
        </div>
    );
}
export default Login;
