/*
 * @Author: AiLjx
 * @Date: 2022-05-15 21:54:57
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 19:21:53
 */
import { BrowserRouter, Link, Routes, Route, Navigate } from "react-router-dom";
import Article from "./views/layout/Article";
import Board from "./views/layout/Board";
import NotFound from "./views/NotFound";
import Layout from "./views/Layout";
import Login from "./views/Login";
import About from "./views/About";

//路由配置
function App() {
    return (
        <BrowserRouter>
            <Link to='/'>首页 </Link>
            <Link to='/login'>登录 </Link>
            <Routes>
                {/* 嵌套路由 */}
                <Route path='/main' element={<Layout />}>
                    {/* 默认二级路由 添加index属性 把它自己的path干掉，和父路由共享路径 */}
                    {/* 在一级路由上使用index，它会匹配/ */}
                    <Route index element={<Board />}></Route>
                    {/* 匹配到/main/artice，效果和下面的一样 */}
                    <Route path='article' element={<Article />}></Route>
                    {/* <Route path='/main/article' element={<Article />}></Route> */}
                    {/* 嵌套路由组件会在父路由中的Outlet组件中渲染 */}
                </Route>

                <Route path='/login' element={<Login />}></Route>

                <Route path='/about/:id' element={<About />}></Route>
                <Route path='/about' element={<About />}></Route>

                <Route path='/' element={<Navigate to='/main' />}></Route>
                {/* *号代表其它所有未定义的路由 */}
                <Route path='*' element={<NotFound />}></Route>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
