import { useSearchParams, useParams } from "react-router-dom";
function About() {
    //获取/about?id=116格式的参数
    const [SearchParams] = useSearchParams();
    const SearchParamsId = SearchParams.get("id");

    //获取/about/11（前提是路由配置是：<Route path='/about/:id' element={<About />}></Route>）
    const params = useParams();

    let id = SearchParamsId ? SearchParamsId : params.id;

    return (
        <div>
            about页面 <h1>路由参数：{id}</h1>
        </div>
    );
}
export default About;
