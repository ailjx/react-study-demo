/*
 * @Author: AiLjx
 * @Date: 2022-05-15 21:54:57
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 19:25:45
 */
import { Outlet, useNavigate } from "react-router-dom";
function Layout() {
    const navigate = useNavigate();
    const goArticle = () => {
        // 编程式导航跳转
        navigate("/main/article", { replace: true });
    };
    return (
        <div>
            我是Layout组件，下面是我的嵌套路由组件
            <button onClick={() => goArticle()}>二级路由跳转</button>
            <hr />
            {/* 二级路由出口 */}
            <Outlet></Outlet>
        </div>
    );
}
export default Layout;
