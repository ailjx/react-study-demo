/*
 * @Author: AiLjx
 * @Date: 2022-07-01 20:59:42
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 21:06:35
 */
import { useNavigate } from "react-router-dom";

export default function Center() {
    const navigate = useNavigate();
    return (
        <div>
            Center
            <button
                onClick={() => {
                    localStorage.removeItem("V6Router");
                    navigate("/login");
                }}>
                退出
            </button>
        </div>
    );
}
