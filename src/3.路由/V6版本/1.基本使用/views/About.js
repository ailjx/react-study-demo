/*
 * @Author: AiLjx
 * @Date: 2022-05-14 21:51:29
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 19:54:34
 */
import { useSearchParams, useParams } from "react-router-dom";
function About() {
    //获取/about?id=116格式的参数
    // useSearchParams格式与useState类似
    const [searchParams, setSearchParams] = useSearchParams();
    const searchParamsId = searchParams.get("id");
    // searchParams.has('id') // 判断id参数是否存在
    // setSearchParams({"id":2}) // 页面内用set方法改变路由

    //获取/about/11（前提是路由配置是：<Route path='/about/:占位符' element={<About />}></Route>）
    const params = useParams();

    // let id = searchParamsId || params.id;
    return (
        <div>
            about页面
            <h1>searchParams参数：{searchParamsId}</h1>
            <h1>params参数：{params?.id}</h1>
            <button
                onClick={() => {
                    setSearchParams({ id: "哇哈哈" });
                }}>
                setSearchParams
            </button>
        </div>
    );
}
export default About;
