/*
 * @Author: AiLjx
 * @Date: 2022-05-14 22:39:57
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 21:09:19
 */
//编程式导航

import { useNavigate } from "react-router-dom";

function Login() {
    const navigate = useNavigate();
    function goAbout() {
        // 编程式导航跳转
        // navigate("/about");
        // 路由query Params形式传参
        //不填加历史记录，可以添加额外参数replace开启重定向
        navigate("/about?id=110", { replace: true });
    }
    function goAbout2() {
        // navigate("/about");
        // 路由Params形式传参
        navigate("/about/110", { replace: true });
    }

    function goCenter() {
        localStorage.setItem("V6Router", "ceshi");
        navigate("/center");
    }
    //跳转到关于项
    return (
        <div>
            Login
            <button onClick={goAbout}>SearchParams传参跳转到关于</button>
            <button onClick={goAbout2}>Params传参跳转到关于</button>
            <button onClick={goCenter}>跳转到个人中心</button>
        </div>
    );
}
export default Login;
