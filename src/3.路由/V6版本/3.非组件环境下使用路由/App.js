import { HistoryRouter, history } from "./utils/history";
import { Link, Routes, Route } from "react-router-dom";
//引进路由页面组件
import About from "./views/About";
import Home from "./views/Home";

function App() {
    return (
        //用HistoryRouter替换BrowserRouter
        <HistoryRouter history={history}>
            <Link to='/'>首页 </Link>
            <Link to='/about'>关于 </Link>

            <Routes>
                <Route path='/' element={<Home />}></Route>
                <Route path='/about' element={<About />}></Route>
            </Routes>
        </HistoryRouter>
    );
}

export default App;
