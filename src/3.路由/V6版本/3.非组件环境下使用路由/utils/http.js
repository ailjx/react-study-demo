//模拟非组件环境拿到路由信息，往往在请求拦截器中用的到
import { history } from "./history";

function goHome() {
    setTimeout(() => {
        history.push("/");
    }, 2000);
}

export { goHome };
