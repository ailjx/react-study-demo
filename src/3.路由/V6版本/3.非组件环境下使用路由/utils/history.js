//安装：npm i history

// https://github.com/remix-run/react-router/issues/8264

import { createBrowserHistory } from "history";
// unstable_HistoryRouter暂时不稳定
import { unstable_HistoryRouter as HistoryRouter } from "react-router-dom";
const history = createBrowserHistory();

export { HistoryRouter, history };
