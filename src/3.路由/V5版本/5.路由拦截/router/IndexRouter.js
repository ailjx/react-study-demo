import {
    BrowserRouter as Router,
    Route,
    Switch,
    Link,
    Redirect,
} from "react-router-dom";

import Login from "../views/Login";
import Home from "../views/Home";

function isAuth() {
    return localStorage.getItem("token");
}

function addAuth() {
    return localStorage.setItem("token", "hahhahahahaah");
}

function removeAuth() {
    return localStorage.removeItem("token");
}

export default function IndexRouter() {
    return (
        <Router>
            {/* <NavLink>是<Link>的一个特定版本，会在匹配上当前的url的时候给已经渲染的元素添加参数 */}
            <Link to='/home'>首页</Link>
            <button onClick={addAuth}>添加token</button>
            <button onClick={removeAuth}>移除token</button>
            <Switch>
                {/* 使用render替代component，当路径匹配时会执行render函数， */}

                {/* 直接判断渲染哪个组件，二选一 */}
                <Route
                    path='/home'
                    render={(props) => {
                        // 这里看到Home组件直接是加< />进行实例化了，并没有接收任何props
                        // 所以使用render后，路由对应的组件将不能直接获得Route传来的props（包含history等对象）
                        // 可在render里的函数参数中获得Route的props，然后手动传给路由对应的组件
                        return isAuth() ? (
                            <Home {...props} />
                        ) : (
                            <Login {...props} />
                        );
                    }}
                />

                {/* 不符合条件时重定向 */}
                <Route
                    path='/home'
                    render={() => {
                        return isAuth() ? <Home /> : <Redirect to='/login' />;
                    }}
                />

                {/* 使用render时路由组件不能直接接收Route传的props了 */}
                <Route path='/login' component={Login} />
            </Switch>
        </Router>
    );
}
