import { Route, Redirect, Switch } from "react-router-dom";
//引进路由页面组件
import About from "./layout/About";
import Home from "./layout/Home";
function Layout() {
    return (
        <div>
            Layout页面，以下是我的嵌套路由页面
            {/* 嵌套此页面的路由配置，这些配置也可以配合插槽和Route的render
            写在IndexRouter.js中，Route的render解释见5.路由拦截，嵌套写法见
             dav-react*/}
            <Switch>
                <Route path='/layout/home' component={Home} />
                <Route path='/layout/about' component={About} />
                <Redirect from='/layout' to='/layout/home' />
            </Switch>
        </div>
    );
}
export default Layout;
