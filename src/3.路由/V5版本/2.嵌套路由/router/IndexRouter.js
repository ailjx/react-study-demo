/*
 * @Author: AiLjx
 * @Date: 2022-06-11 18:11:32
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 09:56:28
 */
import React from "react";
import {
    // 重命名
    HashRouter as Router,
    Route,
    Redirect,
    Switch,
} from "react-router-dom";

import About from "../views/About";
import Layout from "../views/Layout";
import NotFound from "../views/NotFound";

export default function IndexRouter() {
    return (
        <Router>
            <Switch>
                <Route path='/layout' component={Layout} />
                <Route path='/about' component={About} />
                <Redirect from='/' to='/layout' exact />
                <Route path='*' component={NotFound} />
            </Switch>
        </Router>
    );
}
