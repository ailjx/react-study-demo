/*
 * @Author: AiLjx
 * @Date: 2022-06-11 16:29:06
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 14:16:38
 */
// 安装：npm i react-router-dom@5
import React from "react";

import IndexRouter from "./router/IndexRouter";
export default function App() {
    return (
        <div>
            {/* 使用封装的路由组件 */}
            <IndexRouter></IndexRouter>
        </div>
    );
}
// 手动修改浏览器地址栏查看效果
