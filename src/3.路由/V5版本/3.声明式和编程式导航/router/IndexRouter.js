import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";

import About from "../views/About";
import Layout from "../views/Layout";
import NotFound from "../views/NotFound";

export default function IndexRouter({ children }) {
    return (
        // BrowserRouter模式路由前面不带#
        <BrowserRouter>
            {/* 为外部NavLink留插槽，因为NavLink必须要在Router内部 */}
            {children}
            <Switch>
                <Route path='/layout' component={Layout} />
                <Route path='/about' component={About} />
                <Redirect from='/' to='/layout' exact />
                <Route path='*' component={NotFound} />
            </Switch>
        </BrowserRouter>
    );
}
