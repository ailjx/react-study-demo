/*
 * @Author: AiLjx
 * @Date: 2022-06-11 19:54:31
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 22:14:41
 */
// 编程式导航
// react-router-dom为函数式组件封装了一个useHistory Hooks，使用它可以进行路由跳转等操作
import { useHistory } from "react-router-dom";

function ProNav(props) {
    const history = useHistory();
    console.log("ProNav：Route props", props);
    const nav = [
        {
            name: "首页",
            path: "/layout/home",
        },
        {
            name: "关于",
            path: "/layout/about",
        },
    ];

    const handleChangePage = (path) => {
        // window.location.href = "#" + path; //原生JS方法

        // 基于Route路由组件向它component内的组件传递的props对象的方法
        // 类组件常这样用
        // props.history.push(path);

        // 基于hooks的方法
        history.push(path);
    };

    return (
        <div>
            <ul>
                <li>这是编程式导航</li>

                {nav.map((item) => (
                    <button
                        key={item.path}
                        onClick={() => handleChangePage(item.path)}>
                        {/* 也可以使用声明式导航 */}
                        {/* <NavLink to={item.path}>{item.name}</NavLink> */}
                        {item.name}
                    </button>
                ))}
            </ul>
        </div>
    );
}

export default ProNav;
// 在1.基本使用中说到<Route></Route>组件会向它component内的组件传递一个props对象，
// 里面包含可以进行路由跳转，获取参数等操作，但是若有一个组件并不直接是Route组件的component
// 时，也想获得Route传来的props来进行路由操作（如类组件，不能使用useHistory，只能靠Route props
// 中的属性进行路由操作）怎么办呢？
// react-router-dom中提供了一个withRouter高阶函数，经过withRouter包裹后的组件就能直接
// 获取Route props了

// 可以在导出时直接包裹，也可以在使用到时再进行包裹（见Layout.js）
// export default withRouter(ProNav);
