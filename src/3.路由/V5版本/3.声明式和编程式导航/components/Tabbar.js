/*
 * @Author: AiLjx
 * @Date: 2022-06-11 19:34:54
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 21:45:59
 */
//导航组件：声明式导航
import { NavLink } from "react-router-dom";
export default function Tabbar() {
    return (
        <div>
            <ul>
                <li>这是声明式导航</li>
                <button>
                    {/* NavLink最终被渲染成a标签且对应的路由被选中时会自动加上一个active的class */}
                    <NavLink to='/layout'>首页</NavLink>
                </button>
                <button>
                    {/* 可以使用activeClassName自定义路由选中时所加的类名 */}
                    <NavLink to='/about' activeClassName='aboutActive'>
                        关于
                    </NavLink>
                </button>
            </ul>
            <hr />
        </div>
    );
}
