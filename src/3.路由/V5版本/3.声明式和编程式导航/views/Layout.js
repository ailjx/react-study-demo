import { Route, Redirect, Switch, withRouter } from "react-router-dom";
import ProNav from "../components/ProNav";
//引进路由页面组件
import About from "./layout/About";
import Home from "./layout/Home";

//  使用withRouter包裹组件，使其能直接获取Route传递的props，避免组件层级过多时，来回传递props的问题
const WithProNav = withRouter(ProNav);
//  小技巧：也可以在ProNav.js文件中直接导出withRouter(ProNav)

function Layout(props) {
    // <Route></Route>组件会向它component内的组件传递一个props对象，里面包含history可以进行路由跳转等操作
    console.log("layout", props);

    return (
        <div>
            {/* 将Route传递的props传递给ProNav */}
            {/* <ProNav {...props}></ProNav> */}
            {/* 也可以使用withRouter包裹组件，使其能直接获取Route传递的props */}
            <WithProNav></WithProNav>
            Layout页面，以下是我的嵌套路由页面
            {/* 嵌套此页面的路由配置 */}
            <Switch>
                <Route path='/layout/home' component={Home} />
                <Route path='/layout/about' component={About} />
                <Redirect from='/layout' to='/layout/home' />
            </Switch>
        </div>
    );
}
export default Layout;
