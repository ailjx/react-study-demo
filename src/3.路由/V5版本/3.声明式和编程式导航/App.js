import IndexRouter from "./router/IndexRouter";
import Tabbar from "./components/Tabbar";
export default function App() {
    return (
        <div>
            <IndexRouter>
                {/* NavLink必须要在Router内部 */}
                <Tabbar></Tabbar>
            </IndexRouter>
        </div>
    );
}
