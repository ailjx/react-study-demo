//导航组件：声明式导航
import { NavLink, useHistory } from "react-router-dom";
export default function Tabbar(props) {
    const history = useHistory();

    const goHome = (id) => {
        history.push({ pathname: "/home", state: { id: id } });
    };

    const goAbout = (id) => {
        history.push(`/about/${id}`);
    };

    const goNotFound = (id) => {
        history.push(`/notfound/?id=${id}`);
    };

    return (
        <div>
            <ul>
                <li>
                    {/* pathname对应路由路径，state为参数对象（state可自定义，建议写为state或query），id为参数名 */}
                    <NavLink to={{ pathname: "/home", state: { id: 5 } }}>
                        路由对象传参
                    </NavLink>
                    <button onClick={() => goHome(5)}> 路由对象传参</button>
                    {/* 刷新页面时，最外层包裹了BrowserRouter时，不会丢失
                    但如果使用的时HashRouter，刷新当前页面时，会丢失state中的数据 */}
                </li>
                <li>
                    <NavLink to='/about/88'>动态路由传参</NavLink>
                    <button onClick={() => goAbout(88)}> 动态路由传参</button>
                </li>
                <li>
                    <NavLink to={`/notfound?id=${99}&name=xiao`}>
                        query传参
                    </NavLink>
                    <button onClick={() => goNotFound(99)}>query传参</button>
                </li>
            </ul>
            <hr />
        </div>
    );
}
