import { useLocation } from "react-router-dom";
export default function Home(props) {
    const loca = useLocation();

    console.log("home", props);
    return (
        <div>
            参数为：
            {/* ?. ES6可选连操作符 */}
            {/* 通过Route路由组件传来的props获取参数 */}
            <h1>{props.location?.state?.id}</h1>
            {/* 使用useLocation Hook获取 */}
            <h1>{loca?.state?.id}</h1>
        </div>
    );
}
