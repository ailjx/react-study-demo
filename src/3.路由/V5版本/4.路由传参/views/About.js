import { useParams } from "react-router-dom";
function About(props) {
    console.log("about", props);
    const params = useParams();

    return (
        <div>
            {/* 通过Route路由组件传来的props获取参数，注意为什么是‘.id’？因为路由配置中占位符是‘:id’ */}
            使用动态路由传参
            <br />
            params参数为：
            <h1> {props.match.params.id}</h1>
            {/* 通过useParams获取参数 */}
            <h1> {params.id}</h1>
        </div>
    );
}
export default About;
