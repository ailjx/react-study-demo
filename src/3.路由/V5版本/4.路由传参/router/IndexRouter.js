import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import About from "../views/About";
import Home from "../views/Home";
import NotFound from "../views/NotFound";

export default function IndexRouter({ children }) {
    return (
        <Router>
            {/* 为外部NavLink留插槽，因为NavLink必须要在Router内部 */}
            {children}
            <Switch>
                <Route path='/home' component={Home} />
                {/* 动态路由，:id为占位符，id为自定义的接收参数的名称
                形如‘/about/88’地址，通过id可获取参数88 */}
                <Route path='/about/:id' component={About} />
                <Route path='/notfound' component={NotFound} />
            </Switch>
        </Router>
    );
}
