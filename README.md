# 通过 Create React App 构建的 react 应用的学习案例

在一个个实战小案例中学习 react 全家桶，笔记写入注释，在研究代码时能够直接进行学习，事半功倍！

包含内容：

-   `React基础`
-   `React高级`
-   `React Hooks`
-   `React-Router`路由
-   `Mobx`全局状态管理
-   `Redux`全局状态管理
-   `Redux-saga`处理 redux 在异步管理中的流程
-   `immutable`修改不可变数据方案
-   `styled-components` css in js 方案
-   `GraphQl`按需请求数据方案
-   `dva`React 轻量级框架
-   `Umi`React 全家桶集一身的可插拔框架
-   `TS基础`
-   `React-TS`React 中使用 TS

## 运行

### `npm install`

### `npm start`

打开 [http://localhost:3000](http://localhost:3000)查看效果

## 学习提示

查看案例效果只需将`src`根目录下`index.js`第 5 行的 `App` 组件路径改为需要查看的案例的路径

<!-- 每个学习案例都是先在 `App.js` 上进行，之后才移入单独的 js 的文件，若想查看各个学习案例的效果只需复制相应 js 文件的内容到 `App.js` 即可。 -->

对于多文件案例，如路由等，每个文件夹是一个完整的学习案例，将文件夹内 `App.js`的路径添加到`src`根目录下`index.js`第 5 行的 `App` 组件路径即可

> 例如：想查看`mobx状态管理->V5版本->基本使用`的案例效果，只需将`mobx状态管理->V5版本->基本使用`下的 `App.js`路径添加到`src`根目录下`index.js`第 5 行的 `App` 组件路径，如下所示：

```js
//src根目录下的index.js
import React from "react";
import ReactDOM from "react-dom/client";
//展示mobx状态管理/基本使用的案例
import App from "./5.mobx状态管理/V5版本/1.基础使用/App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
);
```

对于`ts-react`,它是一个独立的使用 `CRA` 创建的项目,需要独立`start`运行它,它内部案例的查看与主项目类似

> 交流学习，问题反馈：
>
> QQ：2041909905

## 更新记录

### 2022-7-2

-   优化`ts-react`内容
-   项目完结

### 2022-7-1

-   完善`umi-react`内容
-   完善`React扩展`下`3.umi`内容
-   优化`路由/V5版本/基本使用`内容
-   优化`路由/V5版本/声明式和编程式导航`内容
-   增加`路由/V6版本`中`5.withRouter`内容
-   优化`路由/V6版本/路由懒加载`内容
-   增加`路由/V6版本`中`6.useRoutes配置路由`内容

### 2022-6-30

-   增加`React扩展`下`2.dva`内容
-   根目录下增加`dva-react`内容
-   优化`路由/V5版本/嵌套路由`内容
-   增加`React扩展`下`3.umi`内容
-   根目录下增加`umi-react`内容

### 2022-6-29

-   完善根目录`server`内容
-   增加`React扩展`下`1.GraphQL`内容
-   优化`小案例`目录结构
-   增加`小案例`下`3.GraphQl使用`内容

### 2022-6-28

-   增加`redux-saga`内容
-   增加`React补充`内容
-   创建`React扩展`目录
-   根目录下增加后端`node`服务`server`内容，主要演示后端`graphql`的使用

### 2022-6-27

-   增加`ts-react`下`TS路由`和`TS-Redux`内容
-   增加`styled-components`内容
-   增加`单元测试`内容

### 2022-6-25

-   增加单独项目`ts-react`:(在 `react` 中使用 `ts` 的内容,跟 `src` 同级)
-   增加`ts-react`下`TS基础`和`类组件TS`内容
-   增加`ts-react`下`函数组件TS`内容

### 2022-6-24

-   优化`mobx状态管理`目录结构，增加 V5 版本内容，优化 V6 版本注释
-   `基础/9`名称修改为`9.Context跨组件通信`，并添加部分注释

### 2022-6-23

-   增加`immutable`内容
-   `基础/14`名称修改为`14.类组件性能优化`
-   增加`redux状态管理/1`中的注释内容：增加 Mobx 和 Redux 对比

### 2022-6-22

-   增加`redux状态管理`中 6~8 的内容
-   优化`redux状态管理`中 4，5 的代码

### 2022-6-21

-   增加`反向代理`内容
    1. 增加`src/setupProxy.js`
    2. 增加`基础/15.反向代理（跨域）.js`
-   优化目录结构，删除`src/index.css`和`src/app.css`
-   增加`CSSMoudle`内容：`src/4.CSSMoudle`
-   增加`redux状态管理`中 1~5 的内容

### 2022-6-15

-   完善 `V5 版本路由`内容：
    1. 完善`路由传参`
    2. `基本使用`中增添路由模式介绍和 `Route` 组件部分源码解析
    3. `声明式和编程式导航`中增添 `withRouter` 内容

### 2022-6-11

-   增加 `useReducer` 内容：
    1. `Hooks/7`
    2. `小案例/useReducer` 使用
-   修复《`自定义 Hooks 案例`》无法预览问题
-   优化`路由`案例结构，优化 `V6 版本路由`代码，增加 `V5 版本路由`案例
-   增加 `V6 版本路由`内容：
    1. `非组件环境使用路由`
    2. `路由懒加载`
-   增加 `V5 版本路由`内容：
    1. `基本使用`
    2. `嵌套路由`
    3. `声明式和编程式导航`
    4. `路由传参（待完善）`

### 2022-6-11 之前

-   ...

<!-- ### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify) -->
