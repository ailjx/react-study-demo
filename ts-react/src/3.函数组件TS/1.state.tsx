/*
 * @Author: AiLjx
 * @Date: 2022-06-25 20:13:47
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 20:16:18
 */

import React, { useState } from "react";

export default function App() {
    // useState后定义类型
    const [name, setname] = useState<string>("小明");
    return (
        <div>
            {name}
            <button
                onClick={() => {
                    // setname(1); // 报错
                    setname("小刚");
                }}>
                修改
            </button>
        </div>
    );
}
