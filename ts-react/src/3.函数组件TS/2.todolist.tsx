/*
 * @Author: AiLjx
 * @Date: 2022-06-25 19:17:42
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 20:23:02
 */
import React, { useRef, useState } from "react";

export default function App() {
    // 给input绑定该ref需要指定HTMLInputElement类型
    const inputRef = useRef<HTMLInputElement>(null); // 需要设置默认值null
    // 设置状态list类型为存放string的数组
    const [list, setlist] = useState<Array<string>>([]);
    return (
        <div>
            {/* 如果inputRef不指定类型，会报：
            不能将类型“MutableRefObject<undefined>”分配给类型“LegacyRef<HTMLInputElement> | undefined”。*/}
            <input type='text' ref={inputRef} />
            <button
                onClick={() => {
                    // 直接inputRef.current.value TS在检查时会报：对象可能为 "null"的错误
                    //（React.RefObject<HTMLInputElement>.current: HTMLInputElement | null）
                    // 两种解决方式：
                    // 1.ES6可选链运算符
                    console.log(inputRef.current?.value);
                    // 2.TS的类型断言as运算符
                    console.log(
                        // 断言（肯定）inputRef.current类型为HTMLInputElement而不是null
                        (inputRef.current as HTMLInputElement).value
                    );
                    setlist([
                        ...list,
                        (inputRef.current as HTMLInputElement).value,
                        // 这里用inputRef.current?.value就不行了，
                        // 因为推断不出inputRef.current?.value最后结果的类型
                    ]);
                    (inputRef.current as HTMLInputElement).value = "";
                }}>
                添加
            </button>
            <ul>
                {list.map((item, index) => (
                    <li key={index}>{item}</li>
                ))}
            </ul>
        </div>
    );
}
