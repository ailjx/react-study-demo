/*
 * @Author: AiLjx
 * @Date: 2022-06-25 19:38:24
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 20:26:58
 */
import React from "react";

export default function App() {
    return (
        <div>
            App
            {/* <Child name={9}></Child>
                <Child nam='小明'></Child> */}
            {/* 上述都会提示报错 */}
            <Child name='小明'></Child>
            <Child2 name='小红'></Child2>
        </div>
    );
}
interface IProps {
    name?: string; // 可选
}
// 约定属性props类型
// 法一:
function Child(props: IProps) {
    return <div>Child-{props.name}</div>;
}
// 法二:
const Child2: React.FC<IProps> = (props) => {
    return <div>child-{props.name}</div>;
};
