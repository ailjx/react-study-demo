// TS在类中新增了public，private，protected修饰符

// 将之前用得到的发布订阅模式改写成类的用法
// class Bus {
//     // 这个list是存放订阅者的，正常情况下应该是只能在Bus内部用到
//     // 如果外部能修改这个list，这就将带来风险，这样TS中private定义私有属性变得尤其重要
//     list: Array<any> = [];
//     //订阅
//     subscribe(cb: any) {
//         this.list.push(cb);
//     }
//     //发布
//     publish(arg?: any) {
//         this.list.forEach((cb) => {
//             cb && cb(arg);
//         });
//     }
// }
// let bus = new Bus();
// bus.subscribe(() => console.log("订阅者"));
// bus.list = []; // 会发现在外部可以修改Bus内的list，这就会出现风险
// bus.publish(); // 什么也不会打印，因为上面把list清空了，导致订阅者丢失

// ------------------------------------------------
// 使用
class Bus {
    public name = "Bus"; // public定义公有属性，谁都可以使用，与不加任何修饰符时作用一样
    private list: Array<any> = []; //  private定义私有属性，外部和子类都不能使用，只有自己能使用
    protected age = 18; // protected定义只有自己和子类能使用
    //订阅
    subscribe(cb: any) {
        this.list.push(cb);
    }
    //发布
    publish(arg?: any) {
        this.list.forEach((cb) => {
            cb && cb(arg);
        });
    }
}
let bus = new Bus();
bus.subscribe(() => console.log("订阅者"));
bus.name = "BUS";
console.log(bus.name);
// bus.list = []; // 报错
// console.log(bus.list); // 报错
// bus.age = 20; // 报错
// console.log(bus.age); // 报错
bus.publish(); // 订阅者

// Bus的之类
class BusChil extends Bus {
    getAge() {
        // 只能访问父类的公有属性和 protected定义的属性
        console.log(this.age);
        console.log(this.name);
        // console.log(this.list); // 报错
    }
}
let busC = new BusChil();
busC.getAge();

export {};
