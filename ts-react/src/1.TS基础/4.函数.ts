// function test1(a, b) {} // 参数不设置类型会报错
function test1(a: number, b: string) {
    return a;
}
// test1(); // test1设置了两个参数，使用时不传会报错
// let name: string = test1(1, "1"); // 报错，函数返回number，而name需要接收的是string
let name: number = test1(1, "1");
console.log(name);

// 规定返回值类型为string，c参数可以不传
function test2(a: number, b: string, c?: number): string {
    // return a; // 报错，函数括号后规定的是string，代表返回值必须是string
    return b;
}
test2(1, "2");

// --------------------------------------------------------------
// 使用接口定义函数形态
interface IFunc {
    (a: number, b: string, c?: number): string;
}

// let test3: IFunc = function (a: string, b: string, c?: number): string {
//     return b;
// }; // 报错，接口IFunc规定a参数需要为number类型

let test3: IFunc = function (a: number, b: string, c?: number): string {
    return b;
};
test3(1, "2", 3);

// -------------------------------------------------------------
// 对象内函数类型
interface IObj {
    name: string;
    age: number;
    getName: (name: string) => string;
}
let obj: IObj = {
    name: "小明",
    age: 18,
    getName: (name: string) => {
        // return 9; // 报错
        return name;
    },
};
console.log(obj.getName("9"));

export {};
