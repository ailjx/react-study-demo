/*
 * @Author: AiLjx
 * @Date: 2022-06-25 18:11:38
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 19:03:22
 */
interface IClass {
    getName: () => string;
}
class A implements IClass {
    // 类A以接口IClass为原型，A上缺少了接口IClass中定义的属性和方法时会报错
    getName() {
        return "A";
    }
    // 但类A上可以有原型接口IClass中没有定义的
    getAge() {
        return 18;
    }
}
class B {
    getAge() {
        return 18;
    }
}
let obj1 = new A();
let obj2 = new B();
function getName(obj: IClass) {
    console.log(obj.getName());
}
getName(obj1); // A
// getName(obj2); // 报错：类型“B”的参数不能赋给类型“IClass”的参数。类型 "B" 中缺少属性 "getName"，但类型 "IClass" 中需要该属性。
export {};
