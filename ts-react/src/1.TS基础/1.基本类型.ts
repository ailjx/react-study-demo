// 使用前，需先在ts-react根目录下打开终端npm install安装依赖

// TypeScript 的定位是静态类型语言，在写代码阶段就能检查错误，而非运行阶段
// TS类型系统是最好的文档，增加了代码的可读性和可维护性。
// ts最后被编译成js

// 变量未声明类型时会按照初始值规定自身类型
let a = [];
a.push(6);
// a.substring(1, 2); // 报错（TS会自动检查类型，并提示你）

// string
// String(原生的构造函数) vs string (ts中的类型)
let str: string = "小明";
str.substring(1, 2);

// boolean
let mybool: boolean = false;
// mybool = 1; // 报错
mybool = true;
console.log(mybool);

// number
let mynumber: number = 100;
// mynumber = "1"; // 报错
mynumber = 1;
console.log(mynumber);

// 可能是多个类型用|分割
let myname3: string | number = "小贱";
myname3 = "小红";
myname3 = 99;
console.log(myname3);

// any类型，相当与无任何限制的原生JS类型
let any: any = 6;
any = "6";
any = true;

// ts文件必须要有导出
export {};
