let list = ["1", "2", "3", 4];
console.log(list);

// for (const i in list) {
//     list[i].substring(0, 1); // 报错，因为list中有number类型
// }

// 声明方式一：
// :Array声明数组 <string>数组内元素为string类型
let mylist: Array<string> = ["111", "222", "3333"];
// mylist.push(9); // 报错
console.log(mylist);
// 数组内元素为string或number类型
let mylist2: Array<string | number> = ["111", "222", "3333"];
mylist2.push(1);
// -----------------------------------------------------------
// 声明方式二：
// :string[]内元素为string类型的数组
let list2: string[] = ["111", "222", "3333"];
// list2.push(3); // 报错
console.log(list2);

// 数组内元素为string或number类型
let list3: (string | number)[] = ["111", "222", "3333"];
list3.push(9);

export {};
