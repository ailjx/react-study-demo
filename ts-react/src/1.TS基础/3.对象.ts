// interface接口，定义对象的形状
interface IObj {
    name: string;
    age: number;
    love: string | number;
    father?: string; // 可选属性
}
// 按照IObj接口规定对象内容
const obj: IObj = {
    name: "小红",
    // age: "18", // 报错，因为IObj接口中规定age字段需要为number
    age: 18,
    // like: "睡觉", // 报错，因为IObj接口中没有指定该字段
    love: 8, // 不加love会报错，因为IObj接口中规定要有love，但不加father不会报错，因为它是可选属性
};
console.log(obj.age);

// 如果一个对象内字段很多，但我们只需要规定其中一部分类型，怎么办呢？
interface IObj2 {
    name: string;
    [propName: string]: any; // 只有name属性规定了类型，其它所有的属性不关心，设置为any
}
// 按照IObj接口规定对象内容
const obj2: IObj2 = {
    name: "小红",
    age: 18,
    like: "睡觉",
};
console.log(obj2);

export {};
