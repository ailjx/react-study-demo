/*
 * @Author: AiLjx
 * @Date: 2022-06-27 14:20:04
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 17:56:23
 */
// 安装：npm i react-router-dom@5 npm i @types/react-router-dom@5
// @types/react-router-dom@5是react-router的TS声明文件
import React from "react";
import IndexRouter from "./router";
export default function App() {
    return (
        <div>
            <IndexRouter></IndexRouter>
        </div>
    );
}
