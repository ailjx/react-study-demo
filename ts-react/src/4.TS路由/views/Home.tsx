/*
 * @Author: AiLjx
 * @Date: 2022-06-27 16:54:27
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 17:32:38
 */
import React, { useEffect, useState } from "react";
import axios from "axios";
// 引入路由组件Route传的props类型声明文件
import { RouteComponentProps } from "react-router-dom";

// list状态类型
interface IListObj {
    id: number;
    name: string;
    cover: string;
    [propName: string]: any;
}
export default function Home(props: RouteComponentProps) {
    const [list, setlist] = useState<Array<IListObj>>([]);
    useEffect(() => {
        console.log("Home");

        axios
            .get("https://netease-cloud-music-api.vercel.app/mv/first?limit=10")
            .then((res) => {
                setlist(res.data.data);
            });
    }, []);
    return (
        <div>
            <ul>
                {list.map((item) => (
                    <li
                        key={item.id}
                        onClick={() => {
                            props.history.push(`/detail/${item.id}`);
                        }}>
                        <img src={item.cover} alt={item.name} width='200' />
                        {item.name}
                    </li>
                ))}
            </ul>
        </div>
    );
}
