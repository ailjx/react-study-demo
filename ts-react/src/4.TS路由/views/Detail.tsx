/*
 * @Author: AiLjx
 * @Date: 2022-06-27 16:54:56
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 17:49:27
 */
import React, { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios from "axios";
interface IParam {
    id: string;
}

export default function Detail(props: RouteComponentProps<IParam>) {
    const [url, seturl] = useState<string>("");
    useEffect(() => {
        // 直接使用console.log(props.match.params.id); // 报：类型“{}”上不存在属性“id”，TS不知道你设置的占位符名称
        // 解决方法一：as 断言
        // console.log((props.match.params as any).id);
        // 解决方法二：定义pramas类型IParam，并在RouteComponentProps中使用
        console.log(props.match.params.id);

        axios
            .get(
                `https://netease-cloud-music-api.vercel.app/mv/url?id=${props.match.params.id}`
            )
            .then((res) => {
                seturl(res.data.data.url);
            });
    }, [props.match.params.id]);

    return (
        <div>
            Detail:
            <br />
            <video src={url} controls autoPlay width={600}>
                您的浏览器不支持 video 标签。
            </video>
        </div>
    );
}
