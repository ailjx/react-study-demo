/*
 * @Author: AiLjx
 * @Date: 2022-06-27 16:54:09
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 17:33:03
 */
import React from "react";
import { HashRouter, Route, Redirect, Switch } from "react-router-dom";
import Home from "../views/Home";
import Detail from "../views/Detail";
export default function IndexRouter() {
    return (
        <HashRouter>
            <Switch>
                <Route path='/home' component={Home}></Route>
                <Route path='/detail/:id' component={Detail}></Route>
                <Redirect from='/' to='/home'></Redirect>
            </Switch>
        </HashRouter>
    );
}
