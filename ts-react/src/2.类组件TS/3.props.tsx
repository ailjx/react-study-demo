/*
 * @Author: AiLjx
 * @Date: 2022-06-25 19:38:24
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 19:53:43
 */
import React, { Component } from "react";

export default class App extends Component {
    render() {
        return (
            <div>
                App
                {/* <Child name={9}></Child>
                <Child nam='小明'></Child> */}
                {/* 上述都会提示报错 */}
                <Child name='小明'></Child>
            </div>
        );
    }
}
interface IProps {
    name?: string; // 可选
}
// 约定属性props类型
class Child extends Component<IProps, any> {
    render() {
        return (
            <div>
                Child：
                <h1>{this.props.name}</h1>
            </div>
        );
    }
}
