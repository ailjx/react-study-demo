/*
 * @Author: AiLjx
 * @Date: 2022-06-25 19:04:57
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 19:12:25
 */
import React, { Component } from "react";
interface IState {
    name: string;
}
//Component后进行类型约定:<约定属性props,约定状态>
export default class App extends Component<any, IState> {
    state = {
        // name:6 // 报错
        name: "小明",
    };
    render() {
        return (
            <div>
                <h1>{this.state.name}</h1>
                <button
                    onClick={() => {
                        this.setState({
                            // name:6 // 报错
                            name: "小红",
                        });
                    }}>
                    修改
                </button>
            </div>
        );
    }
}
