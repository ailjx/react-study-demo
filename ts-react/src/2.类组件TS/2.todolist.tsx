/*
 * @Author: AiLjx
 * @Date: 2022-06-25 19:17:42
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-25 19:35:46
 */
import React, { Component } from "react";
interface IState {
    list: string[];
}
export default class App extends Component<any, IState> {
    state = {
        list: [],
    };
    // 给input绑定该ref需要指定HTMLInputElement类型
    inputRef = React.createRef<HTMLInputElement>();
    render() {
        return (
            <div>
                {/* 如果inputRef不指定类型，会报：
                不能将类型“RefObject<unknown>”分配给类型“LegacyRef<HTMLInputElement> | undefined” */}
                <input type='text' ref={this.inputRef} />
                <button
                    onClick={() => {
                        // 直接this.inputRef.current.value TS在检查时会报：对象可能为 "null"的错误
                        //（React.RefObject<HTMLInputElement>.current: HTMLInputElement | null）
                        // 两种解决方式：
                        // 1.ES6可选链运算符
                        console.log(this.inputRef.current?.value);
                        // 2.TS的类型断言as运算符
                        console.log(
                            // 断言（肯定）this.inputRef.current类型为HTMLInputElement而不是null
                            (this.inputRef.current as HTMLInputElement).value
                        );
                        this.setState({
                            list: [
                                ...this.state.list,
                                (this.inputRef.current as HTMLInputElement)
                                    .value,
                                // 这里用this.inputRef.current?.value就不行了，
                                // 因为推断不出this.inputRef.current?.value最后结果的类型
                            ],
                        });
                        (this.inputRef.current as HTMLInputElement).value = "";
                    }}>
                    添加
                </button>
                <ul>
                    {this.state.list.map((item, index) => (
                        <li key={index}>{item}</li>
                    ))}
                </ul>
            </div>
        );
    }
}
