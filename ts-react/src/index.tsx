/*
 * @Author: AiLjx
 * @Date: 2022-06-24 19:06:58
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-02 14:07:13
 */
/*
 * 脚手架创建ts项目：create-react-app 项目名 --template typescript
 * 使用前，需先在ts-react根目录下打开终端npm install安装依赖
 */
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./5.TS-Redux/App";
import reportWebVitals from "./reportWebVitals";
// 测试TS基础时引入
// import "./1.TS基础/6.类+接口";
const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
);
root.render(
    // <React.StrictMode>
    <App />
    // </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
