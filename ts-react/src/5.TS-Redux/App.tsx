/*
 * @Author: AiLjx
 * @Date: 2022-06-27 17:52:39
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 18:04:56
 */
import React, { useState, useEffect } from "react";
import Home from "./Home";
import store from "./redux/store";

export default function App() {
    const [isShow, setisShow] = useState(store.getState().isShow);
    useEffect(() => {
        store.subscribe(() => {
            setisShow(store.getState().isShow);
        });
    }, []);

    return (
        <div>
            {isShow && <h1>App标题</h1>}
            <Home></Home>
        </div>
    );
}
