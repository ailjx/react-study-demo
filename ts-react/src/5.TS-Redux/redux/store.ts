/*
 * @Author: AiLjx
 * @Date: 2022-06-27 17:52:39
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 18:06:50
 */
import { createStore } from "redux";
interface IAction {
    type: string;
    payload?: any;
}
interface IState {
    isShow: boolean;
}
const state = {
    isShow: true,
};

const reducer = (prevState: IState = state, action: IAction) => {
    let newState = { ...prevState };
    switch (action.type) {
        case "hide":
            newState.isShow = false;
            return newState;
        case "show":
            newState.isShow = true;
            return newState;
        default:
            return prevState;
    }
};

const store = createStore(reducer);
export default store;
