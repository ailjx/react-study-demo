/*
 * @Author: AiLjx
 * @Date: 2022-06-27 17:52:39
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-27 17:54:34
 */
import React from "react";
import store from "./redux/store";
export default function Home() {
    const hide = () => {
        store.dispatch({
            type: "hide",
        });
    };

    const show = () => {
        store.dispatch({
            type: "show",
        });
    };

    return (
        <div>
            <button onClick={hide}>隐藏标题</button>
            <button onClick={show}>显示标题</button>
        </div>
    );
}
