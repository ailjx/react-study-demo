/*
 * @Author: AiLjx
 * @Date: 2022-06-30 09:53:10
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 14:00:08
 */
import React from "react";
import { connect } from "dva";
import Tabbar from "../components/Tabbar";

function App({ children, isShow }) {
  // isShow是connect传来的react-redux状态
  return (
    <div style={{ paddingBottom: "50px" }}>
      App
      {/* 接收嵌套路由 */}
      {children}
      {isShow && <Tabbar></Tabbar>}
    </div>
  );
}
// 使用dva版的react-redux
export default connect((state) => {
  return {
    isShow: state.tabbar.isShow,
  };
})(App);
