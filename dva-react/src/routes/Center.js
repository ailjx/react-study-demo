/*
 * @Author: AiLjx
 * @Date: 2022-06-30 10:09:14
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 15:40:21
 */
import { useEffect } from "react";
import { withRouter } from "dva/router";
import request from "../utils/request";

export default function Center() {
  useEffect(() => {
    // 请求有跨域限制的请求会报错
    // request(
    //   "https://i.maoyan.com/api/mmdb/movie/v3/list/hot.json?ct=%E8%A5%BF%E5%8D%8E&ci=936&channelId=4"
    // ).then((res) => {
    //   console.log(res);
    // });

    // 在dva-react根目录下的.webpackrc文件中配置跨域
    request(
      "/api/mmdb/movie/v3/list/hot.json?ct=%E8%A5%BF%E5%8D%8E&ci=936&channelId=4"
    ).then((res) => {
      console.log(res);
    });

    // 使用本地测试的mock接口
    // 在dva-react根目录下的mock文件夹中定义接口，在.roadhogrc.mock.js中导入引用
    request("/user").then((res) => {
      console.log("用户信息", res.data);
    });
  }, []);

  return (
    <div>
      Center
      <WithChild></WithChild>
    </div>
  );
}

function Child({ history }) {
  function goOut() {
    localStorage.removeItem("dva-token");
    history.push("/login");
  }
  return <button onClick={() => goOut()}>退出登录</button>;
}
// 使用路由高阶组件withRouter向组件内传递路由props
const WithChild = withRouter(Child);
