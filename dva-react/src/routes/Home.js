/*
 * @Author: AiLjx
 * @Date: 2022-06-30 10:08:17
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 14:42:41
 */

import { useState, useEffect } from "react";
// 请求都封装在services目录下
import { getHomeList } from "../services/mv";

// 这是普通请求数据列表的方法，结合redux实现列表缓存的方法见Cinema.js
export default function Home({ history }) {
  const [list, setlist] = useState([]);
  useEffect(() => {
    // 获取Home数据列表请求
    getHomeList().then((res) => setlist(res));
  }, []);

  return (
    <div>
      {list.map((item) => (
        <li
          key={item.id}
          onClick={() => {
            history.push(`/detail/${item.id}`);
          }}
        >
          <img src={item.cover} alt={item.name} width="200" />
          {item.name}
        </li>
      ))}
    </div>
  );
}
