/*
 * @Author: AiLjx
 * @Date: 2022-06-30 10:37:08
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 14:42:06
 */
import { useState, useEffect } from "react";
import { connect } from "dva";
import { getMvUrl } from "../services/mv";

// 只要用dva的react-redux的connect包装了，无论connect传不传参数，组件
// 都会接收一个dispatch props
function Detail({ match, dispatch }) {
  const [url, seturl] = useState("");
  useEffect(() => {
    // console.log(match.params.id);

    // 调用命名空间为tabbar的redux的hide reducer
    dispatch({
      type: "tabbar/hide",
    });

    // 获取mv播放地址请求
    getMvUrl(match.params.id).then((res) => seturl(res));

    return () => {
      dispatch({
        type: "tabbar/show",
      });
    };
  }, [match.params.id]);

  return (
    <div>
      Detail:
      <br />
      <video src={url} controls autoPlay width={"100%"}>
        您的浏览器不支持 video 标签。
      </video>
    </div>
  );
}

export default connect()(Detail);
