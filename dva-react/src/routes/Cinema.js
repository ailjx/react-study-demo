/*
 * @Author: AiLjx
 * @Date: 2022-06-30 10:08:17
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 14:48:18
 */

import { useEffect } from "react";
import { connect } from "dva";

function Cinema({ history, mvlist, dispatch }) {
  useEffect(() => {
    if (mvlist.length === 0) {
      // 调起redux-saga，发送请求
      dispatch({
        type: "CinemaList/getList",
      });
    } else {
      console.log("Cinema-->缓存");
    }
  }, []);

  return (
    <div>
      {mvlist.map((item) => (
        <li
          key={item.id}
          onClick={() => {
            history.push(`/detail/${item.id}`);
          }}
        >
          <img src={item.picUrl} alt={item.name} width="200" />
          {item.name}
        </li>
      ))}
    </div>
  );
}
const mapStateToProps = (state) => ({ mvlist: state.CinemaList.list });
export default connect(mapStateToProps)(Cinema);
