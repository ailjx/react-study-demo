/*
 * @Author: AiLjx
 * @Date: 2022-06-30 13:16:59
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 15:58:27
 */
import { useRef } from "react";
import request from "../utils/request";

export default function Login({ history }) {
  function login() {
    // 本地测试mock post接口
    request("/user/login", {
      method: "POST",
      body: JSON.stringify({
        username: username.current.value,
        password: password.current.value,
      }),
      // 定义传的是JSON格式
      headers: {
        "Content-Type": "application/json",
      },
    }).then((res) => {
      if (res.data.ok) {
        localStorage.setItem("dva-token", "dva-token-ceshi");
        history.push("/center");
      } else {
        alert("输入错误！");
      }
    });
  }

  const username = useRef(null);
  const password = useRef(null);
  return (
    <div>
      <p>用户名：ailjx 密码：123</p>
      用户名：
      <input type="text" ref={username} />
      <br />
      密码：
      <input type="password" ref={password} />
      <br />
      <button onClick={() => login()}>登录</button>
    </div>
  );
}
