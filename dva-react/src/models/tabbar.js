/*
 * @Author: AiLjx
 * @Date: 2022-06-30 13:34:01
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 14:07:07
 */

export default {
  // 命名空间
  namespace: "tabbar",

  // 状态
  state: {
    isShow: true,
  },

  // 处理函数
  reducers: {
    hide(prevState, action) {
      return { ...prevState, isShow: false };
    },
    show(prevState, action) {
      return { ...prevState, isShow: true };
    },
  },

  // 订阅函数
  subscriptions: {
    setup({ dispatch, history }) {
      // dispatch reducer，history 路由对象
      // 初始化
      console.log("初始化--tabbar");
    },
  },
};
