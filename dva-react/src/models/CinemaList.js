/*
 * @Author: AiLjx
 * @Date: 2022-06-30 13:34:01
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 14:47:24
 */

import { getCinemaList } from "../services/mv";

export default {
  // 命名空间
  namespace: "CinemaList",

  // 状态
  state: {
    list: [],
  },

  // 处理函数
  reducers: {
    changeList(prevState, { payload }) {
      return { ...prevState, list: payload };
    },
  },

  // 订阅函数
  subscriptions: {
    setup({ dispatch, history }) {
      // dispatch reducer，history 路由对象
      // 初始化
      console.log("初始化--list");
    },
  },

  // 异步--redux-saga
  effects: {
    // 异步处理
    //  * 和yield都是redux-saga中的用法
    // 方法第二个参数是redux-saga对象，可解构出call, put
    *getList(action, { call, put }) {
      // 发其请求，call(promise对象)
      const res = yield call(getCinemaList);
      // 调起新的reducer
      yield put({
        type: "changeList",
        payload: res,
      });
    },
  },
};
