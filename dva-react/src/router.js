/*
 * @Author: AiLjx
 * @Date: 2022-06-30 09:26:55
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 13:29:17
 */
import React from "react";
// 注意来自于dva/router包
import { Router, Route, Switch, Redirect } from "dva/router";
import App from "./routes/App";
import Home from "./routes/Home";
import Cinema from "./routes/Cinema";
import Center from "./routes/Center";
import Detail from "./routes/Detail";
import Login from "./routes/Login";

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Switch>
        {/* login页面并不是App的嵌套页面，应与App同级且在App之上
        （因为路由模糊匹配的原因，放到App下边访问/login时会显示App组件
        ，而无法正常显示Login组件） */}
        <Route path="/login" component={Login}></Route>
        {/* <Route path="/" exact component={App} /> */}
        {/* 使用render写法，便于在App组件内嵌套路由，在App中通过props.children接收插槽内容 */}
        <Route
          path="/"
          render={() => (
            <App>
              <Switch>
                <Route path="/home" component={Home}></Route>
                <Route path="/cinema" component={Cinema}></Route>
                <Route
                  path="/center"
                  render={() =>
                    localStorage.getItem("dva-token") ? (
                      <Center></Center>
                    ) : (
                      <Redirect to="/login" />
                    )
                  }
                ></Route>
                <Route path="/detail/:id" component={Detail}></Route>
                <Redirect from="/" to="/home"></Redirect>
              </Switch>
            </App>
          )}
        />
      </Switch>
    </Router>
  );
}

export default RouterConfig;
