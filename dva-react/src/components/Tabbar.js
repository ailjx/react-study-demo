/*
 * @Author: AiLjx
 * @Date: 2022-06-30 10:15:55
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 10:24:41
 */
import React from "react";
// dva的css默认是CSSModule用法
import style from "./Tabbar.css";
// 注意来自于dva/router包
import { NavLink } from "dva/router";

export default function Tabbar() {
  return (
    <footer>
      <ul>
        <li>
          <NavLink to="/home" activeClassName={style.active}>
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/cinema" activeClassName={style.active}>
            Cinema
          </NavLink>
        </li>
        <li>
          <NavLink to="/center" activeClassName={style.active}>
            Center
          </NavLink>
        </li>
      </ul>
    </footer>
  );
}
