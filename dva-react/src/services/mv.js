/*
 * @Author: AiLjx
 * @Date: 2022-06-30 14:25:47
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 14:40:41
 */
import request from "../utils/request";

export function getCinemaList() {
  return request(
    "https://netease-cloud-music-api.vercel.app/personalized/mv"
  ).then((res) => {
    console.log("Cinema-->", res.data.result);
    return res.data.result;
  });
}
export function getHomeList() {
  return request(
    "https://netease-cloud-music-api.vercel.app/mv/first?limit=10"
  ).then((res) => {
    console.log("home-->", res.data.data);
    return res.data.data;
  });
}
export function getMvUrl(id) {
  return request(
    `https://netease-cloud-music-api.vercel.app/mv/url?id=${id}`
  ).then((res) => {
    return res.data.data.url;
  });
}
