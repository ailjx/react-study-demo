/*
 * @Author: AiLjx
 * @Date: 2022-06-30 09:26:55
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 14:20:05
 */
import dva from "dva";
import "./index.css";

// 1. Initialize初始化
// 默认是Hash模式（带#）路由
const app = dva();
// 等价于
// const app = dva({
//   history: require("history").createHashHistory(),
// });

// 使用Browser模式（不带#）路由，已知BUG：在Detail页面刷新后报错页面消失
// const app = dva({
//   history: require("history").createBrowserHistory(),
// });

// 2. Plugins
// app.use({});

// 3. Model

// 注册模块（redux模块）
app.model(require("./models/tabbar").default);
app.model(require("./models/CinemaList").default);

// 4. Router
app.router(require("./router").default);

// 5. Start
app.start("#root");
