/*
 * @Author: AiLjx
 * @Date: 2022-06-30 09:26:55
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 15:38:17
 */
let mockObj = require("./mock/api");
export default {
  ...mockObj,
};
