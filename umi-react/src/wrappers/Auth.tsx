/*
 * @Author: AiLjx
 * @Date: 2022-06-30 18:08:53
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 18:06:38
 */
import { Redirect } from 'umi';

// wrappers目录存放高阶组件，固定写法：
// https://v3.umijs.org/zh-CN/docs/convention-routing#%E6%9D%83%E9%99%90%E8%B7%AF%E7%94%B1

export default function Auth({ children }: any) {
  if (localStorage.getItem('umi-token')) {
    return <>{children}</>;
  }
  return <Redirect to="/login" />;
}
