/*
 * @Author: AiLjx
 * @Date: 2022-07-01 16:26:36
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 17:37:56
 */
import { Effect } from 'dva';
import { Reducer } from 'redux';

interface IState {
  list: Array<string>;
}

interface ICinema {
  namespace: string;
  state: IState;
  effects: {
    getCinemaList: Effect;
  };
  reducers: {
    changeList: Reducer;
    clearList: Reducer;
  };
}
export default <ICinema>{
  namespace: 'cinema',
  state: {
    list: [],
  },
  reducers: {
    changeList(prevState, action) {
      return { ...prevState, list: action.payload };
    },
    clearList(prevState, action) {
      return { ...prevState, list: [] };
    },
  },
  effects: {
    *getCinemaList(action, { call, put }) {
      // 给getList传参要这样传
      const res = yield call(getList, action.payload.id);
      // 而不是getList(action.payload.id)
      yield put({
        type: 'changeList',
        payload: res,
      });
    },
  },
};

const getList = async (id: string) => {
  const res = await fetch(
    `https://m.maizuo.com/gateway?cityId=${id}&ticketFlag=1&k=3798713`,
    {
      headers: {
        'X-Client-Info':
          '{"a":"3000","ch":"1002","v":"5.2.0","e":"1656580721116200340193281","bc":"410700"}',
        'X-Host': 'mall.film-ticket.cinema.list',
      },
    },
  ).then((res) => res.json());

  return res.data.cinemas;
};
