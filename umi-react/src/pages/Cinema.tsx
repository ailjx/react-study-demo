/*
 * @Author: AiLjx
 * @Date: 2022-06-30 17:02:39
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 18:25:21
 */
import { useEffect } from 'react';
import { NavBar, DotLoading } from 'antd-mobile';
import { SearchOutline } from 'antd-mobile-icons';
// 直接使用dva
import { connect } from 'dva';

// 在pages中创建组件后，会自动生成路由，不管你有没有配置（前提是.umirc中没有routes配置）
// 如：这个Cinema.tsx组件创建后，浏览器地址输入http://localhost:8000/cinema
// 就能显示Cinema组件，这就是约定式路由
function Cinema({ history, cityName, cityId, list, dispatch, loading }: any) {
  useEffect(() => {
    if (list.length === 0) {
      dispatch({
        type: 'cinema/getCinemaList',
        payload: {
          id: cityId,
        },
      });
    } else {
      console.log('使用cinema缓存');
    }
  }, []);

  return (
    <div>
      <NavBar
        onBack={() => history.push('/city')}
        backArrow={false}
        back={cityName}
        right={<SearchOutline />}
      >
        测试
      </NavBar>
      <h1>配合dva redux缓存数据</h1>
      {/* 配合dva的loading插件显示加载组件 */}
      {loading && (
        <div style={{ textAlign: 'center', fontSize: 24 }}>
          <DotLoading color="primary" />
        </div>
      )}
      <ul>
        {list.map((item: any) => (
          <li key={item.cinemaId}>{item.name}</li>
        ))}
      </ul>
    </div>
  );
}

export default connect((state: any) => {
  return {
    cityName: state.city.cityName,
    cityId: state.city.cityId,
    list: state.cinema.list,
    // dva的loading插件，在effect请求发送时loading.global为true，发送后为false
    loading: state.loading.global,
  };
})(Cinema);
