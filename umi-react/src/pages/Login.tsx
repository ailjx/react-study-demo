/*
 * @Author: AiLjx
 * @Date: 2022-06-30 18:43:26
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 18:05:28
 */
import { useRef } from 'react';
export default function Login({ history }: any) {
  function login() {
    // 本地测试mock post接口
    fetch('/user/login', {
      method: 'POST',
      body: JSON.stringify({
        username: (username.current as HTMLInputElement).value,
        password: (password.current as HTMLInputElement).value,
      }),
      // 定义传的是JSON格式
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res: any) => {
        if (res.ok) {
          localStorage.setItem('umi-token', 'umi-token-ceshi');
          history.push('/center');
        } else {
          alert('输入错误！');
        }
      });
  }

  const username = useRef<HTMLInputElement>(null);
  const password = useRef<HTMLInputElement>(null);

  return (
    <div>
      <p>用户名：ailjx 密码：123</p>
      用户名：
      <input type="text" ref={username} />
      <br />
      密码：
      <input type="password" ref={password} />
      <br />
      <button onClick={() => login()}>登录</button>
    </div>
  );
}
