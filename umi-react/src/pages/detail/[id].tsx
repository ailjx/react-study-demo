/*
 * @Author: AiLjx
 * @Date: 2022-06-30 17:58:20
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 18:05:47
 */

// 动态路由写法：https://v3.umijs.org/zh-CN/docs/convention-routing#%E5%8A%A8%E6%80%81%E8%B7%AF%E7%94%B1
import { useState, useEffect } from 'react';
import { useParams, useHistory } from 'umi';
// 自己安装antd-mobile：npm install --save antd-mobile
// 并在.umirc.ts中关掉umi自带老版本的antd-mobile
import { NavBar } from 'antd-mobile';

interface IParams {
  id: string;
}

export default function Detail() {
  const [url, seturl] = useState();
  const params = useParams<IParams>();
  const history = useHistory();

  useEffect(() => {
    fetch(`https://netease-cloud-music-api.vercel.app/mv/url?id=${params.id}`)
      .then((res) => res.json())
      .then((res) => {
        seturl(res.data.url);
      });
  }, []);

  return (
    <div>
      <NavBar onBack={() => history.goBack()}>Detail</NavBar>

      <br />
      <video src={url} controls autoPlay width={'100%'}>
        您的浏览器不支持 video 标签。
      </video>
    </div>
  );
}
