/*
 * @Author: AiLjx
 * @Date: 2022-06-30 16:59:24
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 18:04:42
 */
import { Redirect, useLocation, useHistory } from 'umi';
import { CapsuleTabs } from 'antd-mobile';
// 对于嵌套路由，在pages目录中创建文件夹，_layout.tsx默认为该路由首页（布局文件），
// 其它文件既是_layout.tsx组件的嵌套路由文件
// 如：访问/home显示Home组件(没有设置重定向时)，访问/home/all在Home组件中显示All组件
export default function Home({ children }: any) {
  const loc = useLocation();

  const his = useHistory();
  if (loc.pathname === '/home') {
    return <Redirect to="/home/first"></Redirect>;
  }

  const changeTab = (key: string) => {
    console.log(key);
    his.push(`/home/${key}`);
  };

  return (
    <div>
      <CapsuleTabs
        onChange={changeTab}
        activeKey={loc.pathname.includes('first') ? 'first' : 'all'}
      >
        <CapsuleTabs.Tab title="最新" key="first"></CapsuleTabs.Tab>
        <CapsuleTabs.Tab title="全部" key="all"></CapsuleTabs.Tab>
      </CapsuleTabs>

      {/* 需要为二级路由留显示的插槽 */}
      {children}
    </div>
  );
}
