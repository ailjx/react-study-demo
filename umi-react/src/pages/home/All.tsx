/*
 * @Author: AiLjx
 * @Date: 2022-06-30 17:21:52
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 18:04:28
 */
import { useEffect, useState } from 'react';
import { useHistory } from 'umi';

export default function All() {
  const [list, setlist] = useState([]);
  // 这里使用的hooks的写法，使用props中的history也行
  const history = useHistory();

  useEffect(() => {
    fetch('https://netease-cloud-music-api.vercel.app/mv/all')
      .then((res) => res.json())
      .then((res) => {
        setlist(res.data);
      });
  }, []);

  return (
    <div>
      {list.map((item: any) => (
        <li
          key={item.id}
          onClick={() => {
            history.push(`/detail/${item.id}`);
          }}
        >
          <img src={item.cover} alt={item.name} width="200" />
          {item.name}
        </li>
      ))}
    </div>
  );
}
