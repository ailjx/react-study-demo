/*
 * @Author: AiLjx
 * @Date: 2022-06-30 17:02:51
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 19:21:28
 */
import { useEffect } from 'react';
import { useHistory } from 'umi';

function Center() {
  useEffect(() => {
    // 请求有跨域限制的请求会报错
    // fetch(
    //   'https://i.maoyan.com/api/mmdb/movie/v3/list/hot.json?ct=%E8%A5%BF%E5%8D%8E&ci=936&channelId=4',
    // )
    //   .then((res) => res.json())
    //   .then((res) => {
    //     console.log(res);
    //   });

    // 在umi-react根目录下的.umirc.ts文件中配置跨域
    fetch(
      '/api/mmdb/movie/v3/list/hot.json?ct=%E8%A5%BF%E5%8D%8E&ci=936&channelId=4',
    )
      .then((res) => res.json())
      .then((res) => {
        console.log('跨域接口', res);
      });

    // 使用本地测试的mock接口
    // 在umi-react根目录下的mock文件夹中定义接口即可
    fetch('/user')
      .then((res) => res.json())
      .then((res) => {
        console.log('用户信息', res);
      });
  }, []);

  return (
    <div>
      Center
      <Child></Child>
    </div>
  );
}
// wrappers目录存放高阶组件，固定写法：
// https://v3.umijs.org/zh-CN/docs/convention-routing#%E6%9D%83%E9%99%90%E8%B7%AF%E7%94%B1
// 表示要给Center组件套一个爹：Auth
// 此时要显示Center需要在/wrappers目录下的Auth中进行插槽控制
Center.wrappers = ['@/wrappers/Auth'];
export default Center;

function Child() {
  const history = useHistory();
  function goOut() {
    localStorage.removeItem('umi-token');
    history.push('/login');
  }
  return <button onClick={() => goOut()}>退出登录</button>;
}
