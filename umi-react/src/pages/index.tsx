/*
 * @Author: AiLjx
 * @Date: 2022-06-30 17:12:09
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 17:28:57
 */

// 在umi中引入重定向组件
import { Redirect } from 'umi';
export default function Index() {
  return <Redirect to="/home"></Redirect>;
}
