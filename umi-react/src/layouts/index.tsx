/*
 * @Author: AiLjx
 * @Date: 2022-06-30 17:33:33
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-30 20:13:03
 */
import React from 'react';
import { NavLink } from 'umi';
import './index.less';

// 约定式路由下，pages同级下的layouts目录下的index.tsx文件会作为项目
// 整体的layout文件，记得放插槽
export default function Indexlayout({ children, location }: any) {
  if (location.pathname.includes('/detail') || location.pathname === '/city') {
    return <> {children}</>;
  }
  return (
    <div className="main">
      {children}
      <footer>
        <ul>
          <li>
            <NavLink to="/home" activeClassName="active">
              Home
            </NavLink>
          </li>
          <li>
            <NavLink to="/cinema" activeClassName="active">
              Cinema
            </NavLink>
          </li>
          <li>
            <NavLink to="/center" activeClassName="active">
              Center
            </NavLink>
          </li>
        </ul>
      </footer>
    </div>
  );
}
