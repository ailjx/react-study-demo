/*
 * @Author: AiLjx
 * @Date: 2022-06-30 16:52:01
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-07-01 18:14:43
 */
import { defineConfig } from 'umi';

export default defineConfig({
  // history: {
  //   type: 'hash',
  // }, // hash模式路由
  nodeModulesTransform: {
    type: 'none',
  },
  // routes存在时就会启用配置式路由，路由以routes内配置为准

  // 将routes注释掉，umi就会启用符合umi规则的约定式路由，在pages中新建Cinema.jsx文件
  // 并在文件内导出Cinema组件，umi就会自动生成/cinema路由匹配Cinema组件
  // pages中index.tsx为首页文件，404.tsx为路由匹配不到时显示的组件
  // pages中创建的文件夹可作为嵌套路由使用，见pages/home/_layout.tsx
  // 在pages同级创建layouts目录，里面的index.tsx就会作为整体项目的layout（首次创建layouts需要重启服务器生效）
  // 如果更改了pages目录下的路由结构，约定式路由没有生效，重启服务器试试

  // routes: [{ path: '/', component: '@/pages/home' }],

  // fastRefresh: {},

  // 跨域（反向代理）配置
  proxy: {
    '/api': {
      target: 'https://i.maoyan.com/',
      changeOrigin: true,
    },
  },
  // 把umi中自带的antd插件集中的mobile模块关掉，避免它影响我们使用新版本的antd-mobile
  antd: {
    mobile: false,
  },

  // dav：
  // 按models目录约定注册 model，无需手动 app.model
  // 文件名即 namespace，可以省去 model 导出的 namespace key
  // 无需手写 router.js，交给 umi 处理，支持 model 和 component 的按需加载
  // 内置 query-string 处理，无需再手动解码和编码
  // 内置 dva-loading 和 dva-immer，其中 dva-immer需通过配置开启(简化 reducer 编写)
});
