const express = require("express");
const { buildSchema } = require("graphql");
const graphqlHttp = require("express-graphql");

// 在sercer目录下打开终端，nodemon .\01-type.js启动

// [String]类型为元素为字符串的数组
// Account和Film是自定义的类型
// 类型后加!，表示必填
// geteFilmDetail(id:Int!)表示接收类型为Int的参数id，!表示必须传，
// 描述语句：query {geteFilmDetail(id:2){id,name} } 表示查询id为2的id和name
var Scchema = buildSchema(`

   type Account{
       name:String,
       age:Int,
       location:String
   }

   type Film{
       id:Int,
       name:String,
       poster:String,
       price:Int
   }

   type Query{
       hello: String,
       getName: String,
       getAge :Int,
       getAllNames:[String],  
       getAllAges:[Int],
       getAccountInfo: Account,
       getNowplayingList:[Film],
       geteFilmDetail(id:Int!):Film
   }
`);

var faskeDb = [
    {
        id: 1,
        name: "1111",
        poster: "http://1111",
        price: 100,
    },
    {
        id: 2,
        name: "2222",
        poster: "http://2222",
        price: 200,
    },
    {
        id: 3,
        name: "3333",
        poster: "http://333",
        price: 300,
    },
];
//处理器
const root = {
    hello: () => {
        // 假设return的都是通过数据库查的
        var str = "hello wolrd";

        return str;
    },
    getName: () => {
        return "小明";
    },
    getAge: () => {
        return 100;
    },

    getAllNames: () => {
        return ["小明", "小红", "小亮"];
    },
    getAllAges() {
        return [19, 20, 200000];
    },

    getAccountInfo() {
        return {
            name: "小明",
            age: 18,
            location: "henan",
        };
    },
    getNowplayingList() {
        return faskeDb;
    },
    // 必须解构
    geteFilmDetail({ id }) {
        console.log(id);

        return faskeDb.filter((item) => item.id === id)[0];
    },
};

var app = express();
app.use(
    "/graphql",
    graphqlHttp({
        schema: Scchema,
        rootValue: root,
        graphiql: true,
    })
);

app.listen(3001);

// 打开服务，在调试器右侧输入描述语句进行测试
// 测试示例：
// query {
//  getNowplayingList {
//     id,
//     name, // 指定需要返回哪些字段
//   }
// }

// query {
//     geteFilmDetail(id:2){
//         id,
//         name
//     }
// }
