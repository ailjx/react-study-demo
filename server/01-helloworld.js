/*
 * @Author: AiLjx
 * @Date: 2022-06-28 21:13:11
 * @LastEditors: AiLjx
 * @LastEditTime: 2022-06-29 20:46:12
 */
// 使用前，需先在sercer根目录下打开终端npm install安装依赖
// 在sercer目录下打开终端，nodemon .\01-helloworld.js启动
const express = require("express");
const { buildSchema } = require("graphql");
const graphqlHttp = require("express-graphql");

// 定义描述语句：字段对应的是处理器中查询数据方法的名字，字段后跟返回数据的类型
var Scchema = buildSchema(`
   type Query{
       hello: String,
       getName: String,
       getAge :Int
   }
`);
//处理器
const root = {
    // 查询数据的方法
    hello: () => {
        // 数据库操作...
        // 假设return的都是通过数据库查的
        var str = "hello wolrd";

        return str;
    },
    getName: () => {
        // 数据库操作...
        return "小明";
    },
    getAge: () => {
        // 数据库操作...
        return 18;
    },
};

var app = express();
// 普通接口
app.use("/home", function (req, res) {
    res.send("home data");
});
app.use("/list", function (req, res) {
    res.send("list data");
});

// graphql接口
app.use(
    "/graphql",
    graphqlHttp({
        schema: Scchema,
        rootValue: root,
        // 开启调试器
        graphiql: true,
    })
);
// 打开服务，在调试器右侧进行测试
// 左侧输入描述语句：query { hello }，仅调用hello，输入query { hello,getName }调用hello和getName
// 这个例子就体现了graphql获取多个资源只用一个请求
app.listen(3001);
